<?php

namespace movi\Localization;

use movi\Caching\CacheProvider;
use movi\Model\Entities\Language;
use Nette\Caching\Cache;
use Nette\Localization\ITranslator;
use Nette\Neon\Neon;
use Nette\Object;

class Translator extends Object implements ITranslator
{

	/**
	 * @var Language
	 */
	private $language;

	/**
	 * @var string
	 */
	private $localeDir;

	/**
	 * @var Language[]
	 */
	private $languages;

	/** @var Cache */
	private $cache;

	public $onTranslate;


	public function __construct($localeDir, CacheProvider $cacheProvider)
	{
		$this->localeDir = $localeDir;
		$this->cache = $cacheProvider->create('translations');
	}


	/**
	 * @param $message
	 * @param null $count
	 * @return mixed|string
	 */
	public function translate($message, $count = NULL)
	{
		if (!$this->language) return $message;

		if (!$this->cache->load($this->language->code)) {
			$dictionary = "{$this->localeDir}/{$this->language->code}.neon";
			$translations = array();

			if (file_exists($dictionary)) {
				$translations = Neon::decode(file_get_contents($dictionary));
			}

			if (!$translations) $translations = array();

			$this->cache->save($this->language->code, $translations, array(
				Cache::FILES => array($dictionary)
			));
		}

		$translations = $this->cache->load($this->language->code);

		if (array_key_exists($message, $translations)) {
			$message = $translations[$message];

			if (is_array($message)) {
				$messages = array();

				foreach ($message as $limit => $message)
				{
					$limits = explode(';', $limit);

					foreach ($limits as $limit)
					{
						$messages[$limit] = $message;
					}
				}

				$message = array_key_exists($count, $messages) ? $messages[$count] : array_pop($messages);
				$message = sprintf($message, $count);
			}
		}

		$this->onTranslate($message, $count);

		return $message;
	}


	/**
	 * @param Language $language
	 * @return $this
	 */
	public function setLanguage(Language $language)
	{
		$this->language = $language;

		return $this;
	}


	/**
	 * @return Language
	 */
	public function getLanguage()
	{
		return $this->language;
	}


    public function setLanguages(array $languages)
    {
        $this->languages = $languages;

        return $this;
    }


    public function getLanguages()
    {
        return $this->languages;
    }


	/**
	 * @return Language|null
	 */
    public function getDefaultLanguage()
    {
	    foreach ($this->languages as $language)
	    {
		    if ($language->default) {
			    return $language;
		    }
	    }

	    return NULL;
    }


} 