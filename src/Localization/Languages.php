<?php

namespace movi\Localization;

use movi\InvalidArgumentException;
use movi\Model\Entities\Language;
use movi\Model\Facades\LanguagesFacade;
use movi\Model\Query;
use Nette\Http\Request;
use Nette\Object;

class Languages extends Object implements ILanguages
{

	/**
	 * @var Language[]
	 */
	private $languages;

	/**
	 * @var Language
	 */
	private $default;

    /**
     * @var Request
     */
    private $request;


	public function __construct(LanguagesFacade $languagesFacade, Request $request)
	{
		$this->languages = $languagesFacade->findActive();
        $this->request = $request;
	}


	/**
	 * @return \movi\Model\Entities\Language[]
	 */
	public function findAll()
	{
		return $this->languages;
	}


	/**
	 * @param $code
	 * @return Language
	 * @throws \movi\InvalidArgumentException
	 */
	public function getLanguage($code)
	{
		return isset($this->languages[$code]) ? $this->languages[$code] : NULL;
	}


	/**
	 * @return Language|null
	 */
	public function getDefaultLanguage()
	{
	    $codes = [];
        $languages = [];
        $default = NULL;

		foreach ($this->languages as $language) {
		    $codes[] = $language->code;
            $languages[$language->code] = $language;

            if ($language->default) $default = $language;
		}

		$detected = $this->request->detectLanguage($codes);

        if (!$detected) {
            return $default;
        } else {
            return $languages[$detected];
        }
	}

} 