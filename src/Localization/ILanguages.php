<?php

namespace movi\Localization;

use movi\Model\Entities\Language;

interface ILanguages
{

	/**
	 * @return Language[]
	 */
	public function findAll();


	/**
	 * @param $code
	 * @return Language
	 */
	public function getLanguage($code);

    /**
     * @return Language
     */
    public function getDefaultLanguage();

} 