<?php

namespace movi\Commands\Cache;

use movi\Caching\CacheProvider;
use Nette\Caching\Cache;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CleanCommand extends Command
{

	protected function configure()
	{
		$this->setName('cache:clean')
			->setDescription('Cleans the cache');
	}


	protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
	{
		/** @var CacheProvider $cacheProvider */
		$cacheProvider = $this->getHelper('container')->getByType('movi\Caching\CacheProvider');

		$cache = $cacheProvider->create('test');
		$cache->clean(array(
			Cache::ALL => true
		));

		$outputInterface->write('Cache was cleaned.');
	}

} 