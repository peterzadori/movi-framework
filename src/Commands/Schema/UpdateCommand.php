<?php

namespace movi\Commands\Schema;

use movi\Model\Schema\SchemaTool;
use movi\Schemas\Schema;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends Command
{

    /**
     * @var SchemaTool
     */
    private $schemaTool;


    public function __construct(SchemaTool $schemaTool)
    {
        parent::__construct();

        $this->schemaTool = $schemaTool;
    }


	protected function configure()
	{
		$this->setName('schema:update')
			->setDescription('Update database schema');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
        $schemaTool = $this->schemaTool;

        $schemaTool->onExecuteQuery[] = function($query) use ($output) {
            $output->writeln("<fg=green;options=bold>Executing SQL:</fg=green;options=bold> <fg=yellow;options=bold>$query</fg=yellow;options=bold>");
        };

        $schemaTool->updateSchema();
	}

} 