<?php

namespace movi\Assets\DI;

use movi\DI\CompilerExtension;
use movi\Packages\Providers\IAssetsProvider;
use Nette\DI\Config\Helpers;
use Nette\DI\ContainerBuilder;
use Nette\Utils\Finder;
use WebLoader\Nette\CompilationException;
use WebLoader\Nette\Extension;

class AssetsExtension extends CompilerExtension
{

    public function getDefaultConfig()
    {
        return array(
            'jsDefaults' => array(
                'sourceDir' => '%wwwDir%/js',
                'tempDir' => '%wwwDir%/' . Extension::DEFAULT_TEMP_PATH,
                'tempPath' => Extension::DEFAULT_TEMP_PATH,
                'files' => array(),
                'remoteFiles' => array(),
                'filters' => array(),
                'fileFilters' => array(),
                'joinFiles' => TRUE,
                'namingConvention' => '@' . $this->prefix('jsNamingConvention'),
            ),
            'cssDefaults' => array(
                'sourceDir' => '%wwwDir%/css',
                'tempDir' => '%wwwDir%/' . Extension::DEFAULT_TEMP_PATH,
                'tempPath' => Extension::DEFAULT_TEMP_PATH,
                'files' => array(),
                'remoteFiles' => array(),
                'filters' => array(),
                'fileFilters' => array(),
                'joinFiles' => TRUE,
                'namingConvention' => '@' . $this->prefix('cssNamingConvention'),
            ),
            'js' => array(

            ),
            'css' => array(

            ),
        );
    }


    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->getDefaultConfig());

        $packages = $this->compiler->extensions;
        krsort($packages);

        foreach ($packages as $extension)
        {
            if ($extension instanceof IAssetsProvider) {
                $config = Helpers::merge($config, $extension->getAssets());
            }
        }

        $builder->addDefinition($this->prefix('cssNamingConvention'))
            ->setFactory('WebLoader\DefaultOutputNamingConvention::createCssConvention');

        $builder->addDefinition($this->prefix('jsNamingConvention'))
            ->setFactory('WebLoader\DefaultOutputNamingConvention::createJsConvention');

        $builder->parameters['webloader'] = $config;

        $loaderFactoryTempPaths = array();

        foreach (array('css', 'js') as $type) {
            foreach ($config[$type] as $name => $wlConfig) {
                $wlConfig = Helpers::merge($wlConfig, $config[$type . 'Defaults']);
                $this->addWebLoader($builder, $type . ucfirst($name), $wlConfig);
                $loaderFactoryTempPaths[strtolower($name)] = $wlConfig['tempPath'];

                if (!is_dir($wlConfig['tempDir']) || !is_writable($wlConfig['tempDir'])) {
                    throw new CompilationException(sprintf("You must create a writable directory '%s'", $wlConfig['tempDir']));
                }
            }
        }

        $builder->addDefinition($this->prefix('factory'))
            ->setClass('WebLoader\Nette\LoaderFactory', array($loaderFactoryTempPaths, $this->name));
    }

    private function addWebLoader(ContainerBuilder $builder, $name, $config)
    {
        $filesServiceName = $this->prefix($name . 'Files');

        $files = $builder->addDefinition($filesServiceName)
            ->setClass('WebLoader\FileCollection')
            ->setArguments(array($config['sourceDir']));

        foreach ($config['files'] as $file) {
            // finder support
            if (is_array($file) && isset($file['files']) && (isset($file['in']) || isset($file['from']))) {
                $finder = Finder::findFiles($file['files']);

                if (isset($file['exclude'])) {
                    $finder->exclude($file['exclude']);
                }

                if (isset($file['in'])) {
                    $finder->in(is_dir($file['in']) ? $file['in'] : $config['sourceDir'] . DIRECTORY_SEPARATOR . $file['in']);
                } else {
                    $finder->from(is_dir($file['from']) ? $file['from'] : $config['sourceDir'] . DIRECTORY_SEPARATOR . $file['from']);
                }

                foreach ($finder as $foundFile) {
                    /** @var \SplFileInfo $foundFile */
                    $files->addSetup('addFile', array($foundFile->getPathname()));
                }

            } else {
                $files->addSetup('addFile', array($file));
            }
        }

        $files->addSetup('addRemoteFiles', array($config['remoteFiles']));

        $compiler = $builder->addDefinition($this->prefix($name . 'Compiler'))
            ->setClass('WebLoader\Compiler')
            ->setArguments(array(
                '@' . $filesServiceName,
                $config['namingConvention'],
                $config['tempDir'],
            ));

        $compiler->addSetup('setJoinFiles', array($config['joinFiles']));

        foreach ($config['filters'] as $filter) {
            $compiler->addSetup('addFilter', array($filter));
        }

        foreach ($config['fileFilters'] as $filter) {
            $compiler->addSetup('addFileFilter', array($filter));
        }

        // todo css media
    }


    /*
    public function loadConfiguration()
    {
        $assets = $this->getConfig($this->defaults);
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('loaderFactory'))
            ->setClass('movi\Assets\LoaderFactory');

        foreach ($this->compiler->extensions as $extension)
        {
            if ($extension instanceof IAssetsProvider) {
                if (method_exists($extension, 'getAssets')) {
                    $assets = array_merge_recursive($assets, $extension->getAssets());
                }
            }
        }

        foreach(['css', 'js'] as $type)
        {
            $namespaces = $assets[$type];

            foreach ($namespaces as $namespace => $files)
            {
                $collection = $builder->addDefinition($this->prefix("$type.$namespace.collection"))
                    ->setClass('movi\Assets\FileCollection', [$builder->expand('%wwwDir%')]);

                $files = $files['files'];

                foreach ($files as $file)
                {
                    $collection->addSetup('addFile', [$file]);
                }

                $builder->addDefinition($this->prefix("$type.$namespace.compiler"))
                    ->setClass('movi\Assets\Compiler', [$collection, $builder->expand('%wwwDir%'), $builder->expand('%assetsDir%'), $builder->expand('%debugMode%')]);
            }
        }
    }
    */

}