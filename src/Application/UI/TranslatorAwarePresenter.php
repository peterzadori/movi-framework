<?php

namespace movi\Application\UI;

use Nette\Localization\ITranslator;

interface TranslatorAwarePresenter
{

	/**
	 * @return ITranslator
	 */
	public function getTranslator();

} 