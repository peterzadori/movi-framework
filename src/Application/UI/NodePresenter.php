<?php

namespace movi\Application\UI;

use movi\Tree\Node;

abstract class NodePresenter extends Presenter
{

	/**
	 * @var Node
	 * @persistent
	 */
	public $node;


	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->node = $this->node;
	}

} 