<?php

namespace movi\Application\UI;

use movi\Components\Flashes\Flash;
use movi\Components\Flashes\Flashes;
use movi\Components\Flashes\IFlashesFactory;
use movi\Components\Head\IHeadFactory;
use movi\Components\Widgets\WidgetsPresenterTrait;
use movi\Localization\ILanguages;
use movi\Localization\Translator;
use movi\Security\User;
use Nette\Application\UI\Multiplier;
use WebLoader\FileCollection;
use WebLoader\Filter\CssUrlsFilter;
use WebLoader\Nette\LoaderFactory;

/**
 * Class Presenter
 * @package movi\Application\UI
 * @property-read $templateFactory TemplateFactory
 * @property-read User $user
 */
abstract class Presenter extends \Nette\Application\UI\Presenter implements TranslatorAwarePresenter
{

    use WidgetsPresenterTrait;


    /**
     * @persistent
     */
    public $lang;

    /**
     * @var Translator
     * @inject
     */
    public $translator;

    /**
     * @var ILanguages
     * @inject
     */
    public $languages;

    /**
     * @var LoaderFactory
     * @inject
     */
    public $webLoader;

    /**
     * @var TemplateFactory
     * @inject
     */
    public $templateFactory;

    /**
     * @var IFlashesFactory
     * @inject
     */
    public $flashesFactory;

    /**
     * @var IHeadFactory
     * @inject
     */
    public $headFactory;


    public function flashMessage($message, $type = Flash::SUCCESS, $title = NULL, $close = true)
    {
        return $this['flashes']->flashMessage($message, $type, $title, $close);
    }


    /**
     * @return Flashes
     */
    protected function createComponentFlashes()
    {
        return $this->flashesFactory->create();
    }


    protected function createComponentHead()
    {
        return $this->headFactory->create();
    }


    /**
     * @return Multiplier
     */
    protected function createComponentJs()
    {
        return new Multiplier(function($namespace = 'front') {
            $loader = $this->webLoader->createJavaScriptLoader($namespace);

            $this->assetsJsFiles($loader->getCompiler()->getFileCollection());

            if (!$this->isDebugMode()) {
                /*
                $loader->getCompiler()->addFileFilter(function($code) {
                    return Minifier::minify($code, array(
                        'flaggedComments' => false
                    ));
                });
                */
            }

            return $loader;
        });
    }


    protected function assetsJsFiles(FileCollection $collection)
    {

    }


    /**
     * @return Multiplier
     */
    protected function createComponentCss()
    {
        return new Multiplier(function($namespace = 'front') {
            $loader = $this->webLoader->createCssLoader($namespace);

            $this->assetsCssFiles($loader->getCompiler()->getFileCollection());

            $filter = new CssUrlsFilter($this->context->parameters['wwwDir'], $this->template->basePath);
            $loader->getCompiler()->addFileFilter(array($filter, '__invoke'));

            return $loader;
        });
    }


    protected function assetsCssFiles(FileCollection $collection)
    {

    }


    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }


    /**
     * @return \Nette\Http\IRequest
     */
    public function getHttpRequest()
    {
        return parent::getHttpRequest();
    }


    /**
     * @return ILanguages
     */
    public function getLanguages()
    {
        return $this->languages;
    }


    /**
     * @return mixed
     */
    public function isDebugMode()
    {
        return $this->context->parameters['debugMode'];
    }

} 
