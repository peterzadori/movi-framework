<?php

namespace movi\Application\UI;

use Nette\Application\UI\Control as NetteControl;
use Nette\Bridges\ApplicationLatte\TemplateFactory as NetteTemplateFactory;
use Nette\Localization\ITranslator;

class TemplateFactory extends NetteTemplateFactory
{

	public $onCreate;


	/** @var ITranslator */
	private $translator;


	/**
	 * @param Control $control
	 * @return \Nette\Bridges\ApplicationLatte\Template
	 */
	public function createTemplate(NetteControl $control = NULL)
	{
		$template = parent::createTemplate($control);
		$template->setTranslator($this->translator);

		$this->onCreate($template, $this);

		return $template;
	}


	public function setTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}

}
