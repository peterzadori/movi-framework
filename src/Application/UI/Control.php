<?php

namespace movi\Application\UI;

use movi\Security\User;

/**
 * Class Control
 * @package movi\Application\UI
 *
 * @property-read Presenter $presenter
 * @property-read User $user
 */
abstract class Control extends \Nette\Application\UI\Control
{

	public $onAttach;


	public function __construct()
	{
		parent::__construct();
	}


	public function render()
	{
		$this->beforeRender();

		$this->template->render();

		$this->afterRender();
	}


	public function attached($parent)
	{
		parent::attached($parent);

		$this->onAttach($parent);
	}


	public function beforeRender()
	{

	}


	public function afterRender()
	{

	}


	/**
	 * @return \movi\Security\User
	 */
	public function getUser()
	{
		return $this->presenter->getUser();
	}


	/**
	 * @return bool
	 */
	public function isAjax()
	{
		return $this->presenter->isAjax();
	}


	/**
	 * @return \Nette\Templating\FileTemplate|\Nette\Templating\ITemplate
	 */
	public function createTemplate()
	{
		$template = parent::createTemplate();
		$reflection = $this->getReflection();
		$file = dirname($reflection->fileName) . '/' . $reflection->getShortName() . '.latte';

		if (file_exists($file)) {
			$template->setFile($file);
		}

		return $template;
	}

} 