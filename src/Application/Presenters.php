<?php

namespace movi\Application;

use movi\Application\UI\Presenter;
use Nette\Object;

final class Presenters extends Object
{

	/**
	 * @var Presenter[]
	 */
	private $presenters = array();

	/**
	 * @var array
	 */
	private $metadata = array();


	/**
	 * @param Presenter $presenter
	 * @param $tags
	 */
	public function addPresenter(Presenter $presenter, $tags)
	{
		$name = $presenter->getReflection()->getName();

		$this->presenters[$name] = $presenter;
		$this->metadata[$name] = $tags;
	}


	/**
	 * @return UI\Presenter[]
	 */
	public function getPresenters()
	{
		return $this->presenters;
	}


	/**
	 * @param $presenter
	 * @return array
	 */
	public function getMetadata($presenter)
	{
		return $this->metadata[$presenter];
	}

} 