<?php

namespace movi\Application\Routers;

use movi\InvalidArgumentException;
use movi\Localization\ILanguages;
use movi\Localization\Translator;
use Nette\Application\Routers\Route;
use Nette\Http\IRequest;

class LanguageRoute extends Route
{

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var ILanguages
	 */
	private $languages;

	const ROUTE_MASK = '[<lang>/]';


	public function __construct($mask, $metadata = array(), Translator $translator, ILanguages $languages)
	{
		$this->translator = $translator;
		$this->languages = $languages;

		if (count($languages->findAll()) > 1) {
			$mask = self::ROUTE_MASK . $mask;
		}

		parent::__construct($mask, $metadata);
	}


	/**
	 * @param IRequest $httpRequest
	 * @return \Nette\Application\Request|NULL
	 */
	public function match(IRequest $httpRequest)
	{
		$request = parent::match($httpRequest);

		if ($request === NULL) return NULL;

		$params = $request->parameters;

		if (array_key_exists('lang', $params)) {
			$language = $this->languages->getLanguage($params['lang']);

			if ($language !== NULL) {
				$this->translator->setLanguage($language);
			}

			$params['lang'] = $this->translator->getLanguage()->code;
		}

		$request->setParameters($params);

		return $request;
	}

} 