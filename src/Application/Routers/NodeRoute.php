<?php

namespace movi\Application\Routers;

use movi\Localization\ILanguages;
use movi\Localization\Translator;
use movi\Tree\Node;
use movi\Tree\Tree;
use Nette\Application\Request;
use Nette\Http\IRequest;
use Nette\Http\Url;

class NodeRoute extends LanguageRoute
{

	/**
	 * @var string
	 */
	private $_module;

	/**
	 * @var string
	 */
	private $_presenter;

	/**
	 * @var string
	 */
	private $_action;

	/**
	 * @var Tree
	 */
	private $tree;


	public function __construct($module, $presenter, $action = 'default', $prefix = NULL, Tree $tree, Translator $translator, ILanguages $languages)
	{
		$this->_module = $module;
		$this->_presenter = $presenter;
		$this->_action = $action;

		$this->tree = $tree;

		parent::__construct(
			$prefix . "[<node .*>][/<module qzyuiopzy>]",
			array(
				'module' => $this->_module,
				'presenter' => $this->_presenter,
				'action' => $this->_action
			), $translator, $languages
		);
	}


	/**
	 * @param IRequest $httpRequest
	 * @return Request|NULL
	 */
	public function match(IRequest $httpRequest)
	{
		$request = parent::match($httpRequest);

		if ($request === NULL) return NULL;

		$params = $request->parameters;

		if (array_key_exists('node', $params)) {
			$node = $this->tree->findByPath($params['node']);

			if ($node === NULL) return NULL;

			$this->modifyMatchRequest($request, $node);
		}

		return $request;
	}


	/**
	 * @param Request $request
	 * @param Node $node
	 * @return Request
	 */
	private function modifyMatchRequest(Request $request, Node $node)
	{
		$params = $request->parameters;
		$params['node'] = $node;

		$request->setParameters($params);

		return $request;
	}


	/**
	 * @param Request $request
	 * @param Url $url
	 * @return NULL|string
	 */
	public function constructUrl(Request $request, Url $url)
	{
		$params = $request->parameters;

		if (array_key_exists('node', $params) && $params['node'] !== NULL && $request->getPresenterName() === "$this->_module:$this->_presenter") {
			/** @var Node $node */
			$node = $params['node'];

			if (!$node instanceof Node) {
				$node = $this->tree->findByPath($node);
			}

			if ($node === NULL) return NULL;

			$path = $node->getPath();
			$route = ($path === NULL) ? NULL : $path . '/';

			$this->modifyConstructRequest($request, $route);
		}

		return parent::constructUrl($request, $url);
	}


	/**
	 * @param Request $request
	 * @param $node
	 */
	private function modifyConstructRequest(Request $request, $node)
	{
		$params = $request->parameters;

		$params['node'] = $node;
		$params['module'] = $this->_module;
		$params['presenter'] = $this->_presenter;
		$params['action'] = $this->_action;

		$request->parameters = $params;

		$request->setPresenterName("$this->_module:$this->_presenter");
	}

} 