<?php

namespace movi\Security;

use movi\Model\Facades\UsersFacade;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;

class Authenticator
{

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var UsersFacade
	 */
	private $usersFacade;


	public function __construct(User $user, UsersFacade $usersFacade)
	{
		$this->user = $user;
		$this->usersFacade = $usersFacade;
	}


	public function login($email, $password)
	{
		try {
			$user = $this->usersFacade->login($email, $password);
		} catch (AuthenticationException $e) {
			throw new AuthenticationException('Zle zadané údaje.');
		}

		$identity = new Identity($user);
		$this->user->login($identity);
	}

} 