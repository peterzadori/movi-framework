<?php

namespace movi\Security;

use movi\Model\DAOFactory;
use movi\Model\Query;
use Nette\Http\Session;
use Nette\Http\UserStorage as NUserStorage;

class UserStorage extends NUserStorage
{

	/**
	 * @var DAOFactory
	 */
	private $DAOFactory;


	public function __construct(Session $sessionHandler, DAOFactory $DAOFactory)
	{
		parent::__construct($sessionHandler);

		$this->DAOFactory = $DAOFactory;
	}


	public function getIdentity()
	{
		/** @var Identity $identity */
		$identity = parent::getIdentity();

		if ($identity !== NULL && $identity instanceof Identity) {
			if ($identity->getEntity() === NULL) {
				$query = new Query();
				$query->where('@id', $identity->id);

				$repository = $this->DAOFactory->getDao($identity->getEntityClass());
				$entity = $repository->find($query);

				$identity->setEntity($entity);
			}
		}

		return $identity;
	}

} 