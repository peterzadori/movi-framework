<?php

namespace movi\Security;

use Nette\Security\User as NetteUser;

/**
 * Class User
 * @package movi\Security
 *
 * @property-read Identity $identity
 */
class User extends NetteUser
{

	public function getEntity()
	{
		$identity = $this->getIdentity();

		return $identity ? $identity->getEntity() : NULL;
	}

} 