<?php

namespace movi\Security;

use movi\Model\Entities\User as UserEntity;
use movi\Model\Entities\UserGroup;
use Nette\Security\IAuthorizator;
use Nette\Security\IResource;

class Authorizator implements IAuthorizator
{

    /**
     * @param $role
     * @param $resource
     * @param $privilege
     * @return bool
     */
    public function isAllowed($role, $resource, $privilege)
    {
        if ($role === UserEntity::SUPERADMIN) return true;
        if (!$role instanceof UserGroup) return false;
        if (!$resource instanceof IResource) return false;

        $permissions = $role->getPermissions();

        return $this->checkPermission($resource, $permissions);
    }


    /**
     * @param IResource $resource
     * @param array $permissions
     * @return bool
     */
    private function checkPermission(IResource $resource, array $permissions)
    {
        $resourceId = $resource->getResourceId();

        if (!array_key_exists($resourceId, $permissions)) {
            return false;
        } else {
            return $permissions[$resourceId];
        }
    }

}