<?php

namespace movi\Security;

use Nette\Object;
use Nette\Security\IIdentity;

/**
 * Class Identity
 * @package movi\Security
 *
 * @property-read int $id
 * @property User $user
 * @property IUserEntity $entity
 */
class Identity extends Object implements IIdentity
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var array
	 */
	private $roles;

	/**
	 * @var IUserEntity
	 */
	private $entity;

	private $entityClass;


	public function __construct(IUserEntity $entity)
	{
		$this->setEntity($entity);
	}


	public function setEntity(IUserEntity $entity)
	{
		$this->entity = $entity;
		$this->id = $entity->getId();
		$this->roles = $entity->getRoles();
		$this->entityClass = get_class($entity);
	}


	public function __sleep()
	{
		return array('id', 'entityClass');
	}


	/**
	 * @return int|mixed
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * @return array
	 */
	public function getRoles()
	{
		return $this->roles;
	}


	/**
	 * @return IUserEntity
	 */
	public function getEntity()
	{
		return $this->entity;
	}


	public function getEntityClass()
	{
		return $this->entityClass;
	}

} 