<?php

namespace movi\Security;

interface IUserEntity
{

	public function getId();

	public function getRoles();

}