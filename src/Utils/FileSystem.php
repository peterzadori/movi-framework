<?php

namespace movi\Utils;

use Nette\Utils\FileSystem as NetteFileSystem;

class FileSystem extends NetteFileSystem
{

	public static function ensureFile($file)
	{
		if (!file_exists($file)) {
			self::write($file, '');
		}
	}

} 