<?php

namespace movi\Templating;

use movi\Files\Utils\FileLinker;
use movi\Files\Utils\ThumbnailCreator;
use Nette\Object;

class FileFilters extends Object
{

    /**
     * @var FileLinker
     */
    private $fileLinker;

    /**
     * @var ThumbnailCreator
     */
    private $thumbnailCreator;


    public function __construct(FileLinker $fileLinker, ThumbnailCreator $thumbnailCreator)
    {
        $this->fileLinker = $fileLinker;
        $this->thumbnailCreator = $thumbnailCreator;
    }


    public function getFile($key)
    {
        return $this->fileLinker->getRelativePath($key);
    }


    public function getThumb($args)
    {
        $defaults = array('width' => NULL, 'height' => NULL, 'flag' => 'exact');
        $args = array_merge($defaults, $args);

        $thumbnail = $this->thumbnailCreator->create($args['key'], $args['width'], $args['height'], $args['flag']);

        if ($thumbnail) {
            return $this->fileLinker->getRelativePath($thumbnail->getKey());
        }
    }

}