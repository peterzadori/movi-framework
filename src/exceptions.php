<?php

namespace movi;

class InvalidArgumentException extends \Exception
{

}

class EntityNotFound extends InvalidArgumentException
{

	public $sql;


	public function __construct($message, $sql = NULL)
	{
		parent::__construct($message);

		$this->sql = $sql;
	}

}


class FormErrorException extends InvalidArgumentException
{

}