<?php

namespace movi;

use movi\Packages\Providers\IConfigProvider;
use movi\Packages\Providers\IExtensionProvider;
use Nette\Configurator as NetteConfigurator;
use Nette\Utils\FileSystem;

class Configurator extends NetteConfigurator
{

	const PACKAGE_CONFIG_FILE = '/Resources/config/package.neon';

	/**
	 * @return \Nette\DI\Container
	 */
	public function createContainer()
	{
		$this->setup();

		return parent::createContainer();
	}


	private function setup()
	{
		$this->addParameters([
			'wwwDir' => dirname(realpath($_SERVER['SCRIPT_FILENAME']))
		]);

		$this->addParameters([
			'tempDir' => $this->parameters['wwwDir'] . '/app/temp',
			'logDir' => $this->parameters['wwwDir'] . '/app/log',
			'packagesDir' => $this->parameters['wwwDir'] . '/app/packages',
			'assetsDir' => $this->parameters['wwwDir'] . '/assets',
			'themesDir' => $this->parameters['wwwDir'] . '/themes',
			'storageDir' => $this->parameters['wwwDir'] . '/storage',
			'metadataDir' => $this->parameters['wwwDir'] . '/storage/metadata',
			'localeDir' => $this->parameters['wwwDir'] . '/app/locale'
		]);

		$this->addExtension('movi', 'movi\DI\Extensions\moviExtension');
		$this->addExtension('packages', 'movi\DI\Extensions\PackagesExtension');
		$this->addExtension('events', 'Kdyby\Events\DI\EventsExtension');
		$this->addExtension('console', 'Kdyby\Console\DI\ConsoleExtension');
		$this->addExtension('replicator', 'Kdyby\Replicator\DI\ReplicatorExtension');
		$this->addExtension('webloader', 'movi\Assets\DI\AssetsExtension');

		FileSystem::createDir($this->parameters['logDir']);
		FileSystem::createDir($this->parameters['packagesDir']);
		FileSystem::createDir($this->parameters['themesDir']);
		FileSystem::createDir($this->parameters['storageDir']);
		FileSystem::createDir($this->parameters['localeDir']);

		$this->setTempDirectory($this->parameters['tempDir']);
		$this->enableDebugger($this->parameters['logDir']);
	}


	/**
	 * @param array $packages
	 */
	public function registerPackages(array $packages)
	{
		foreach ($packages as $package)
		{
			$this->addExtension(get_class($package), get_class($package));

			// Register package.neon
			$reflection = $package->getReflection();
			$packageDir = dirname($reflection->getFileName());

			if (file_exists($packageDir . self::PACKAGE_CONFIG_FILE) && !$package instanceof IConfigProvider) {
				$this->addConfig($packageDir . self::PACKAGE_CONFIG_FILE);
			}

			if ($package instanceof IConfigProvider) {
				foreach ($package->getConfigFiles() as $file)
				{
					$this->addConfig($file);
				}
			}

			if ($package instanceof IExtensionProvider) {
				foreach ($package->getExtensions() as $name => $extension)
				{
					$this->addExtension($name, $extension);
				}
			}
		}
	}


	/**
	 * @param $name
	 * @param $extension
	 */
	public function addExtension($name, $extension)
	{
		$this->defaultExtensions[$name] = $extension;
	}

} 