<?php

namespace movi;

use movi\Packages\IPackage;
use Nette\DI\Container;
use Nette\Object;

class Booter extends Object
{

	/**
	 * @var IPackage[]
	 */
	private $packages;

	/**
	 * @var Configurator
	 */
	private $configurator;

	/**
	 * @var Container
	 */
	private $container;


	public function __construct()
	{
		$this->configurator = new Configurator();
		$this->packages = array();
	}


	/**
	 * Boots application
	 */
	final public function boot()
	{
		$this->addConfig(__DIR__ . '/framework.neon');

		// Run Forrest, run!
		$this->getContainer()->getByType('Nette\Application\Application')->run();
	}


	/**
	 * @param IPackage $package
	 * @return $this
	 */
	public function registerPackage(IPackage $package)
	{
		$this->packages[] = $package;

		return $this;
	}


	/**
	 * @param $file
	 * @return $this
	 */
	public function addConfig($file)
	{
		$this->configurator->addConfig($file);

		return $this;
	}


	/**
	 * @param $value
	 * @return $this
	 */
	public function setDebugMode($value)
	{
		$this->configurator->setDebugMode($value);

		return $this;
	}


	/**
	 * @return Configurator
	 */
	public function getConfigurator()
	{
		return $this->configurator;
	}


	/**
	 * @return Container
	 */
	public function getContainer()
	{
		if (!$this->container) {
			$this->configurator->registerPackages($this->packages);

			$this->container = $this->configurator->createContainer();
		}

		return $this->container;
	}

} 