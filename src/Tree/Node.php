<?php

namespace movi\Tree;

use movi\Model\Entities\TreeEntity;
use Nette\Object;

class Node extends Object
{

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var Node
	 */
	protected $parent;

	/**
	 * @var int
	 */
	protected $order;

	/**
	 * @var TreeEntity|NULL
	 */
	protected $entity;

	/**
	 * @var Node[]
	 */
	public $children = [];

	protected $root = false;


	public function __construct(TreeEntity $entity = NULL)
	{
		if ($entity !== NULL) $this->setEntity($entity);
	}


	/**
	 * @param Node $node
	 */
	public function addChild(self $node)
	{
		$node->parent = $this;
		$this->children[$node->id] = $node;
	}


	/**
	 * @return bool
	 */
	public function hasChildren()
	{
		return count($this->children) > 0;
	}


	/**
	 * @param array $children
	 * @return array
	 */
	public function getChildrenRecursively(&$children = [])
	{
		foreach ($this->children as $child)
		{
			$children[] = $child;

			$child->getChildrenRecursively($children);
		}

		return $children;
	}


	/**
	 * @return string
	 */
	public function getPath()
	{
		$path = [];
		$node = $this;

		while (!$node->isRoot())
		{
			$path[] = $node->path;

			$node = $node->parent;
		}

		$path = array_reverse($path);

		return implode('/', $path);
	}


	/**
	 * @return string
	 */
	public function getNamePath()
	{
		$path = [];
		$node = $this;

		while (!$node->isRoot())
		{
			$path[] = $node->name;

			$node = $node->parent;
		}

		$path = array_reverse($path);

		return implode(' / ', $path);
	}


	/**
	 * @return self[]
	 */
	public function getParents()
	{
		$parents = [];
		$parent = $this->parent;

		while (!$parent->isRoot())
		{
			$parents[] = $parent;

			$parent = $parent->parent;
		}

		return $parents;
	}


    public function getSiblings()
    {
        $siblings = $this->parent->children;
        unset($siblings[$this->id]);

        return $siblings;
    }


    public function getPreviousNode()
    {
        $keys = array_keys($this->parent->children);
        $found_index = array_search($this->id, $keys);

        if ($found_index === false || $found_index === 0) {
            return NULL;
        } else {
            return $this->parent->children[$keys[$found_index - 1]];
        }
    }


    public function getNextNode()
    {
        $keys = array_keys($this->parent->children);
        $found_index = array_search($this->id, $keys);

        if ($found_index === false || $found_index === count($keys) - 1) {
            return NULL;
        } else {
            return $this->parent->children[$keys[$found_index + 1]];
        }
    }


	/**
	 * @return \stdClass
	 */
	public function export()
	{
		$data = new \stdClass();

		if ($this->isRoot()) $this->name = 'Hlavný uzol';

		$data->id = $this->id;
		$data->label = $this->name;
		$data->disabled = $this->isRoot();
		$data->children = array();

		foreach ($this->children as $child)
		{
			$data->children[] = $child->export();
		}

		return $data;
	}


	/**
	 * @param TreeEntity $entity
	 */
	protected function setEntity(TreeEntity $entity)
	{
		$this->entity = $entity;

		$this->id = $entity->id;
		$this->name = $entity->name;
		$this->path = $entity->path;
		$this->order = $entity->order;
	}


	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @param $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}


	/**
	 * @return int
	 */
	public function getOrder()
	{
		return $this->order;
	}


	/**
	 * @return Node
	 */
	public function getParent()
	{
		return $this->parent;
	}


	/**
	 * @return TreeEntity|NULL
	 */
	public function getEntity()
	{
		return $this->entity;
	}


	/**
	 * @return bool
	 */
	public function isRoot()
	{
		return $this->root;
	}


	/**
	 * string
	 */
    public function getIcon()
    {
		return '';
    }

} 