<?php

namespace movi\Tree;

use movi\Model\Entities\TreeEntity;
use movi\Model\Facades\TreeFacade;
use Nette\Object;

abstract class Tree extends Object
{

    /**
     * @var TreeFacade
     */
    protected $facade;

    /**
     * @var RootNode
     */
    protected $root;

    /**
     * @var Node[]
     */
    protected $nodes;

    /**
     * @var bool
     */
    protected $built = false;

    /** @var string */
    protected $nodeClass = 'movi\Tree\Node';


    public $onFetch;

    public $onBuild;


    public function __construct(TreeFacade $facade)
    {
        $this->facade = $facade;
        $this->root = new RootNode();
        $this->nodes = [];
    }


    public function build()
    {
        if (!$this->built) {
            $rows = $this->facade->findAll();

            /** @var TreeEntity $row */
            foreach ($rows as $row)
            {
                $node = new $this->nodeClass($row);

                $this->nodes[$node->id] = $node;

                $this->onFetch($this, $row, $node);
            }

            foreach ($rows as $row)
            {
                $node = $this->nodes[$row->id];

                if ($row->getParentId() !== NULL) {
                    $this->nodes[$row->getParentId()]->addChild($node);
                } else {
                    $this->root->addChild($node, $this);
                }
            }

            $this->onBuild($this);

            $this->built = true;
        }
    }


    public function rebuild()
    {
        $this->built = false;
        $this->nodes = [];
        $this->root->children = [];

        $this->build();
    }


    /**
     * @param Node $node
     * @param Node|NULL $parent
     */
    public function addNode(Node $node, Node $parent = NULL)
    {
        $this->build();

        if ($parent === NULL) {
            $parent = $this->root;
        }

        $parent->addChild($node);

        $this->saveNodesOrder();
    }


    /**
     * @param Node $node
     * @param Node|NULL $parent
     * @param Node|NULL $oldParent
     * @param null $position
     */
    public function moveNode(Node $node, Node $parent = NULL, Node $oldParent = NULL, $position = NULL)
    {
        $this->build();

        if (!$parent) {
            $parent = $this->root;
        }

        if ($oldParent === NULL) {
            $oldParent = $this->root;
        }

        if ($oldParent) {
            unset($oldParent->children[$node->id]);
        }

        if ($parent->children) {
            $before = array_slice($parent->children, 0, $position, true);
            $after = array_slice($parent->children, $position, NULL, true);

            $parent->children = $before;
            $parent->addChild($node);
            $parent->children += $after;
        } else {
            $parent->addChild($node);
        }

        $node->entity->parent = $parent->entity;
        $this->facade->persist($node->entity);

        $this->saveNodesOrder();
    }


    /**
     * @param Node $node
     */
    public function removeNode(Node $node)
    {
        $this->build();

        $parent = $node->parent;
        unset($parent->children[$node->id]);

        $this->facade->delete($node->entity);

        foreach ($this->getNodes($node) as $node)
        {
            $parent = $node->parent;
            unset($parent->children[$node->id]);

            $this->facade->delete($node->entity);
        }

        $this->saveNodesOrder();
    }


    /**
     * @param $id
     * @return Node|null
     */
    public function getNode($id)
    {
        $this->build();

        if (isset($this->nodes[$id])) {
            return $this->nodes[$id];
        } else {
            return NULL;
        }
    }


    /**
     * @param Node|NULL $node
     * @param array $nodes
     * @param null $exclude
     * @return array
     */
    public function getNodes(Node $node = NULL, &$nodes = [], $exclude = NULL)
    {
        $this->build();

        if ($node === NULL) {
            $node = $this->root;
        }

        foreach ($node->children as $child)
        {
            if ($child->id === $exclude) continue;

            $nodes[] = $child;

            $this->getNodes($child, $nodes, $exclude);
        }

        return $nodes;
    }


    /**
     * @param $path
     * @return Node|null
     */
    public function findByPath($path)
    {
        $this->build();

        foreach ($this->nodes as $node)
        {
            if ($node->getPath() === $path) {
                return $node;
            }
        }

        return NULL;
    }


    /**
     * @return RootNode
     */
    public function getRoot()
    {
        $this->build();

        return $this->root;
    }


    protected function saveNodesOrder()
    {
        $order = 1;

        foreach ($this->getNodes() as $node)
        {
            $entity = $node->entity;
            $entity->order = $order;

            $this->facade->persist($entity);

            $order++;
        }
    }

}