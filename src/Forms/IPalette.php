<?php

namespace movi\Forms;

use movi\Application\UI\Form;

interface IPalette
{

    public function configure(Form $form);

    public function attach(Form $form);

    public function setFormFactory(FormFactory $factory);

}