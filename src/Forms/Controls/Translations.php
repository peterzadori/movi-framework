<?php

namespace movi\Forms\Controls;

use movi\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\Callback;

class Translations extends Container
{

	/**
	 * @var callable
	 */
	private $callback;

	private $groups;


	public function __construct(callable $callback, $groups = true)
	{
		parent::__construct();

		$this->callback = Callback::check($callback);
		$this->groups = $groups;

        $this->monitor('movi\Application\UI\Form');
	}


	protected function attached($parent)
	{
		parent::attached($parent);

		if ($parent instanceof Form) {
			foreach ($parent->translator->getLanguages() as $language)
			{
				$container = $this->addContainer($language->id);

				if ($this->groups) {
					$group = $this->form->addGroup($language->name);
					$container->setCurrentGroup($group);
				}


				Callback::invokeArgs($this->callback, array($container, $language));
			}
		}

		$this->setCurrentGroup(NULL);
	}


	public function setDefaults($values, $erase = false)
	{
        parent::setDefaults($values);
	}

} 