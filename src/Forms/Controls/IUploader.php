<?php

namespace movi\Forms\Controls;

use Nette\Http\FileUpload;

interface IUploader
{

	public function upload(FileUpload $file);

} 