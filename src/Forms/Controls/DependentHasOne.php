<?php

namespace movi\Forms\Controls;

use movi\Application\UI\Presenter;
use movi\Model\Entities\IdentifiedEntity;
use movi\Model\Query;
use Nette\Forms\IControl;
use Nette\Utils\Callback;

class DependentHasOne extends HasOne
{

    /**
     * @var null
     */
    private $targetEntity;

    /**
     * @var IControl
     */
    protected $dependsOn;


    public function __construct($label, $entity = NULL, $valueGetter = NULL)
    {
        parent::__construct($label, $entity, $valueGetter);

        $this->targetEntity = $entity;
        $this->entity = NULL;
    }


    /**
     * @param IControl $control
     * @return $this
     */
    public function setDependency(IControl $control)
    {
        $this->dependsOn = $control;

        return $this;
    }


    /**
     * @param Query $query
     */
    protected function modifyQuery(Query $query)
    {
        if ($this->queryModifier) {
            Callback::invokeArgs($this->queryModifier, array($query, $this->dependsOn));
        }
    }


    public function loadHttpData()
    {
        parent::loadHttpData();

        $this->entities = NULL;
        $this->entity = NULL;

        if ($this->dependsOn->getValue()) {
            $this->entity = $this->targetEntity;
            $this->fetchEntities();
        }
    }


    public function attached($parent)
    {
        parent::attached($parent);

        if ($parent instanceof Presenter && $this->dependsOn && $this->dependsOn->getValue()) {
            $this->entity = $this->targetEntity;
            $this->fetchEntities();
        }
    }


    /**
     * @return \Nette\Utils\Html
     */
    public function getControl()
    {
        $control = parent::getControl();

        if ($this->dependsOn) {
            $control->data('dependency', '#' . $this->dependsOn->getHtmlId());
        }

        return $control;
    }

}