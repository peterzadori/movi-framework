<?php

namespace movi\Forms\Controls;

use movi\Model\DAOFactory;
use movi\Model\DAORepository;
use movi\Model\Entities\IdentifiedEntity;
use movi\Model\Entities\TreeEntity;
use movi\Model\Entity;
use movi\Model\Query;
use movi\Tree\Node;
use movi\Tree\Tree;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use Nette\Utils\Callback;

trait ModelControl
{

	/**
	 * @var DAOFactory
	 */
	protected $daoFactory;

	/**
	 * @var DAORepository
	 */
	protected $repository;

	/**
	 * @var string
	 */
	protected $entityClass;

	/**
	 * @var Entity[]
	 */
	protected $entities;

	/**
	 * @var callable
	 */
	protected $valueGetter;

	/**
	 * @var callable
	 */
	protected $queryModifier;

    /**
     * @var Tree
     */
    protected $tree;


	/**
	 * @param $callback
	 * @return $this
	 */
	public function setValueGetter(callable $callback)
	{
		$this->valueGetter = Callback::check($callback);

		return $this;
	}


	/**
	 * @param $callback
	 * @return $this
	 */
	public function setQueryModifier(callable $callback)
	{
		$this->queryModifier = Callback::check($callback);

		return $this;
	}


	/**
	 * @param Query $query
	 */
	protected function modifyQuery(Query $query)
	{
		if ($this->queryModifier) {
			Callback::invokeArgs($this->queryModifier, array($query));
		}
	}


	/**
	 * @param array $rows
	 * @return $this
	 */
	public function setEntities(array $rows)
	{
		$entities = array();
		$items = array();

		foreach ($rows as $entity)
		{
			if ($entity instanceof IdentifiedEntity) {
				$value = ($this->valueGetter !== NULL) ?
					Callback::invokeArgs($this->valueGetter, array($entity)) : $entity->name;

				$entities[$entity->id] = $entity;
				$items[$entity->id] = $value;
			} elseif ($entity instanceof Node) {
				$entities[$entity->getId()] = $entity->getEntity();
				$items[$entity->getId()] = $entity->getNamePath();
			}
		}

		$this->entities = $entities;
		$this->setItems($items);

		return $this;
	}


    /**
     * @param Tree $tree
     * @param null $exclude
     * @return $this
     */
    public function setTree(Tree $tree, $exclude = NULL)
    {
        $tree->build();

        $this->tree = $tree;

        $entities = [];
        $nodes = [];

        foreach ($tree->getNodes(NULL, $nodes, $exclude) as $node)
        {
            $entities[] = $node;
        }

        $this->setEntities($entities);

        return $this;
    }


	/**
	 * @param IContainer $parent
	 */
	public function validateParent(IContainer $parent)
	{
		parent::validateParent($parent);

		$this->monitor('Nette\Application\UI\Presenter');
	}


	public function attached($parent)
	{
		parent::attached($parent);

		if ($parent instanceof Presenter && $this->entityClass !== NULL) {
			$this->fetchEntities();
		}
	}


    protected function fetchEntities()
    {
        $query = new Query();
        $query->switchMode($query::COLLECTION_MODE);

        $this->modifyQuery($query);

        $this->daoFactory = $this->getPresenter()->context->getByType('movi\Model\DAOFactory');
        $this->repository = $this->daoFactory->getDao($this->entityClass);

        if (empty($this->entities)) {
            $this->setEntities($this->repository->find($query));
        }
    }


    public function getPresenter()
    {
        return $this->lookup('Nette\Application\UI\Presenter');
    }

} 