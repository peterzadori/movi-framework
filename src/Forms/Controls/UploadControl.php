<?php

namespace movi\Forms\Controls;

use Nette\Application\UI\ISignalReceiver;
use Nette\Forms\Controls\UploadControl as NetteUploadControl;
use Nette\Http\FileUpload;
use movi\Forms\Controls\TemplateControlTrait;

class UploadControl extends NetteUploadControl implements ISignalReceiver
{

	use TemplateControlTrait;


	/** @var IUploader */
	protected $uploader;

	/**
	 * @var bool
	 */
	private $uploaded = false;

	/**
	 * @var string
	 */
	private $key;

	public $onDelete;


	public function __construct($label = NULL, IUploader $uploader)
	{
		parent::__construct($label, false);

		$this->setUploader($uploader);
	}


	public function handleDelete()
	{
		$key = $this->key;
		$this->key = NULL;

		$this->onDelete($key, $this);
	}


	public function getControl()
	{
		$this->getTemplate()->input = parent::getControl();
		$this->getTemplate()->file = $this->key;

		return $this->toString();
	}


	/**
	 * @param IUploader $uploader
	 * @return $this
	 */
	public function setUploader(IUploader $uploader)
	{
		$this->uploader = $uploader;

		return $this;
	}


	public function getValue()
	{
		$file = parent::getValue();

		if (!$this->uploaded && !empty($file->name)) {
			$this->uploaded = true;
			$this->key = $this->uploader->upload($file);

			return $this->key;
		} else {
			return $this->key;
		}
	}


	public function setValue($value)
	{
		$this->key = $value;

		parent::setValue($value);
	}


	/********************* validators ****************d*g**/


	/**
	 * Is file size in limit?
	 * @return bool
	 */
	public static function validateFileSize(NetteUploadControl $control, $limit)
	{
		foreach (static::toArray($control->getFile()) as $file) {
			if ($file->getSize() > $limit || $file->getError() === UPLOAD_ERR_INI_SIZE) {
				return FALSE;
			}
		}
		return TRUE;
	}


	/**
	 * Has file specified mime type?
	 * @return bool
	 */
	public static function validateMimeType(NetteUploadControl $control, $mimeType)
	{
		$mimeTypes = is_array($mimeType) ? $mimeType : explode(',', $mimeType);
		foreach (static::toArray($control->getFile()) as $file) {
			$type = strtolower($file->getContentType());
			if (!in_array($type, $mimeTypes, TRUE) && !in_array(preg_replace('#/.*#', '/*', $type), $mimeTypes, TRUE)) {
				return FALSE;
			}
		}
		return TRUE;
	}


	/**
	 * Is file image?
	 * @return bool
	 */
	public static function validateImage(NetteUploadControl $control)
	{
		foreach (static::toArray($control->getFile()) as $file) {
			if (!$file->isImage()) {
				return FALSE;
			}
		}
		return TRUE;
	}


	/**
	 * @return array
	 */
	private static function toArray($value)
	{
		return $value instanceof FileUpload ? array($value) : (array) $value;
	}


	public function getFile()
	{
		return parent::getValue();
	}

} 