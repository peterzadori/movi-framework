<?php

namespace movi\Forms\Controls;

use movi\Model\Entities\IdentifiedEntity;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\Controls\MultiSelectBox;

final class HasMany extends MultiSelectBox
{

	use ModelControl;


	public function __construct($label, $entityClass = NULL, $valueGetter = NULL)
	{
		$this->entityClass = $entityClass;

		if ($valueGetter) {
			$this->setValueGetter($valueGetter);
		}

		parent::__construct($label);
	}


	/**
	 * @return IdentifiedEntity[]
	 */
	public function getValue()
	{
		$value = parent::getValue();
		$entities = array();

		foreach ($value as $id)
		{
			$entities[] = $this->entities[$id];
		}

		return $entities;
	}


	public function setValue($entities)
	{
		$items = array();

		if (is_array($entities)) {
			foreach ($entities as $entity)
			{
				if ($entity instanceof IdentifiedEntity) {
					$items[$entity->id] = $entity->id;
				} else {
					$items[$entity] = $entity;
				}
			}
		}

		$this->value = $items;
	}

} 