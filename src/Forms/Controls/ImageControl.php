<?php

namespace movi\Forms\Controls;

use Nette\Application\UI\ISignalReceiver;
use Nette\Http\FileUpload;

class ImageControl extends \Nette\Forms\Controls\UploadControl implements ISignalReceiver
{

    use TemplateControlTrait;


    /**
     * @var IUploader
     */
    protected $uploader;

    /**
     * @var bool
     */
    private $uploaded = false;

    /**
     * @var string
     */
    private $key;

    /**
     * @var int
     */
    private $width = 500;

    /**
     * @var string
     */
    private $flag = 'fit';


    public $onDelete;

    public $onUpload;


    public function __construct($label = NULL, IUploader $uploader)
    {
        parent::__construct($label, false);

        $this->setUploader($uploader);
    }


    public function handleDelete()
    {
        $key = $this->key;
        $this->key = NULL;

        $this->onDelete($key, $this);
    }


    public function getControl()
    {
        $template = $this->getTemplate();
        $template->width = $this->width;
        $template->flag = $this->flag;
        $template->key = $this->key;
        $template->input = parent::getControl();

        return $this->toString();
    }


    /**
     * @param IUploader $uploader
     * @return $this
     */
    public function setUploader(IUploader $uploader)
    {
        $this->uploader = $uploader;

        return $this;
    }


    public function getUploader()
    {
        return $this->uploader;
    }


    /**
     * @param $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }


    public function setFlag($flag)
    {
        $this->flag = $flag;
    }


    public function getValue()
    {
        $file = parent::getValue();

        if (!$this->uploaded && !empty($file->name)) {
            $this->uploaded = true;
            $this->key = $this->uploader->upload($file);

            return $this->key;
        } else {
            return $this->key;
        }
    }


    public function setValue($value)
    {
        $this->key = $value;

        parent::setValue($value);
    }


    /********************* validators ****************d*g**/


    /**
     * Is file size in limit?
     * @return bool
     */
    public static function validateFileSize(UploadControl $control, $limit)
    {
        foreach (static::toArray($control->getFile()) as $file) {
            if ($file->getSize() > $limit || $file->getError() === UPLOAD_ERR_INI_SIZE) {
                return FALSE;
            }
        }
        return TRUE;
    }


    /**
     * Has file specified mime type?
     * @return bool
     */
    public static function validateMimeType(UploadControl $control, $mimeType)
    {
        $mimeTypes = is_array($mimeType) ? $mimeType : explode(',', $mimeType);
        foreach (static::toArray($control->getFile()) as $file) {
            $type = strtolower($file->getContentType());
            if (!in_array($type, $mimeTypes, TRUE) && !in_array(preg_replace('#/.*#', '/*', $type), $mimeTypes, TRUE)) {
                return FALSE;
            }
        }
        return TRUE;
    }


    /**
     * Is file image?
     * @return bool
     */
    public static function validateImage(UploadControl $control)
    {
        foreach (static::toArray($control->getFile()) as $file) {
            if (!$file->isImage()) {
                return FALSE;
            }
        }
        return TRUE;
    }


    /**
     * @return array
     */
    private static function toArray($value)
    {
        return $value instanceof FileUpload ? array($value) : (array) $value;
    }


    public function getFile()
    {
        return parent::getValue();
    }

}