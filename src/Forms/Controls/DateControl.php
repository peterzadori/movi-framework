<?php

namespace movi\Forms\Controls;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\DateTime;

class DateControl extends TextInput
{

    /**
     * @var string
     */
    private $format = 'd.m.Y';


    /**
     * @param $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }


    public function getControl()
    {
        $control = parent::getControl();
        $control->addClass('datepicker');

        return $control;
    }


    public function setValue($value)
    {
        if ($value instanceof \DateTime) {
            $value = $value->format($this->format);
        }

        return parent::setValue($value);
    }


    /**
     * @return DateTime
     */
    public function getValue()
    {
        $value = parent::getValue();

        return $value ? DateTime::from(strtotime($value)) : NULL;
    }

}