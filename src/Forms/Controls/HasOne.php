<?php

namespace movi\Forms\Controls;

use movi\Model\Entities\IdentifiedEntity;
use movi\Tree\Node;
use movi\Tree\Tree;
use Nette\Forms\Controls\SelectBox;

class HasOne extends SelectBox
{

	use ModelControl;


	public function __construct($label, $entityClass = NULL, $valueGetter = NULL)
	{
		$this->entityClass = $entityClass;

		if ($valueGetter) {
			$this->setValueGetter($valueGetter);
		}

		parent::__construct($label);
	}


	/**
	 * @param $value
	 * @return \Nette\Forms\Controls\ChoiceControl|void
	 */
	public function setValue($value)
	{
		if ($value instanceof IdentifiedEntity) {
			$value = $value->id;
		}

		$this->value = $value === NULL ? NULL : key(array((string) $value => NULL));

		return $this;
	}


	/**
	 * @return IdentifiedEntity
	 */
	public function getValue()
	{
		$value = parent::getValue();

		if ($value === NULL) return NULL;

		return $this->entities[$value];
	}

} 