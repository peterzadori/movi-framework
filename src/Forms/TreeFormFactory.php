<?php

namespace movi\Forms;

use LeanMapper\IMapper;
use movi\Application\UI\Form;
use movi\Model\Entities\NodeEntity;
use movi\Model\Facade;
use movi\Tree\Node;
use movi\Tree\Tree;

abstract class TreeFormFactory extends TranslationsFormFactory
{

    /**
     * @var Tree
     */
    protected $tree;


    public function postProcessForm(Form $form)
    {
        parent::postProcessForm($form);

        $values = $form->values;
        $parent = NULL;
        $node = $this->tree->getNode($this->entity->id);

        if ($values->parent !== NULL) {
            $parent = $this->tree->getNode($values->parent->id);
        } else {
            $parent = $this->tree->getRoot();
        }

        if ($node && $node->parent->id !== $parent->id) {
            $this->tree->moveNode($node, $parent, $node->getParent(), count($parent->children));
        } elseif (!$node) {
            $node = new Node($this->entity);
            $this->tree->addNode($node, $parent);
        }

        $this->tree->rebuild();
    }

} 