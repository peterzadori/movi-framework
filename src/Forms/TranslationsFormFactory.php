<?php

namespace movi\Forms;

use movi\Application\UI\Form;
use movi\Model\TranslationsFacade;
use Nette\InvalidStateException;

abstract class TranslationsFormFactory extends EntityFormFactory
{

	public function loadValues(Form $form)
	{
        parent::loadValues($form);

		if (!($this->facade instanceof TranslationsFacade)) {
			throw new InvalidStateException();
		}

		if (!$this->entity->isDetached()) {
			$translations = $this->facade->getTranslations($this->entity);

			if ($translations !== NULL && isset($form['translations'])) {
                $form['translations']->setDefaults($translations);
            }
		}
	}


	public function postProcessForm(Form $form)
	{
		$values = $form->getValues();

		$this->persistTranslations($form, $values->translations);

		parent::postProcessForm($form);
	}


	protected function persistTranslations(Form $form, $translations)
	{
		foreach ($form->presenter->getLanguages()->findAll() as $language)
		{
			if (isset($translations[$language->id])) {
				$this->entity->assign($translations[$language->id]);
				$this->entity->language = $language;

				$this->facade->persist($this->entity);
			}
		}
	}

} 