<?php

namespace movi\Forms\Renderers;

use Kdyby\Replicator\Container;
use Nette\Forms\Form;
use Nette\Forms\IControl;
use Nette\Forms\Rendering\DefaultFormRenderer;
use Nette\Forms\Controls;
use Nette\Utils\Html;

class BootstrapFormRenderer extends DefaultFormRenderer
{

    const MODE_VERTICAL = 'vertical',
        MODE_HORIZONTAL = 'horizontal';


    private $mode = self::MODE_VERTICAL;


    /**
     * @param $mode
     * @return $this
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }


    public function render(Form $form, $mode = NULL)
    {
        $this->prepareWrappers($form);
        $this->prepareControls($form);

        return parent::render($form, $mode);
    }



    /**
     * Renders 'control' part of visual row of controls.
     * @return string
     */
    public function renderControl(IControl $control)
    {
        $body = $control->getOption('wrapper')
            ? Html::el($control->getOption('wrapper'))
            : $this->getWrapper('control container');

        if ($this->counter % 2) {
            $body->class($this->getValue('control .odd'), TRUE);
        }

        $description = $control->getOption('description');
        if ($description instanceof Html) {
            $description = ' ' . $description;

        } elseif (is_string($description)) {
            $description = ' ' . $this->getWrapper('control description')->setText($control->translate($description));

        } else {
            $description = '';
        }

        if ($control->isRequired()) {
            $description = $this->getValue('control requiredsuffix') . $description;
        }

        $control->setOption('rendered', TRUE);
        $el = $control->getControl();
        if ($el instanceof Html && $el->getName() === 'input') {
            $el->class($this->getValue("control .$el->type"), TRUE);
        }
        if ($el instanceof Html && $control->getOption('placeholder')) {
            $el->placeholder($control->getOption('placeholder'));
        }

        if (($prepend = $control->getOption('prepend')) !== NULL || ($append = $control->getOption('append')) !== NULL) {
            $group = Html::el('div class=input-group');
            if ($prepend) $group->add(Html::el('span class=input-group-addon')->setText($prepend));
            $group->add($el);
            if (isset($append)) $group->add(Html::el('span class=input-group-addon')->setText($append));

            $el = $group;
        }

        return $body->setHtml($el . $description . $this->renderErrors($control));
    }


    protected function prepareWrappers(Form $form)
    {
        $this->wrappers['error']['container'] = 'div class="alert alert-danger"';
        $this->wrappers['error']['item'] = 'div';
        $this->wrappers['controls']['container'] = NULL;
        $this->wrappers['pair']['container'] = 'div class="row form-group"';
        $this->wrappers['pair']['.error'] = 'has-error';
        $this->wrappers['control']['description'] = 'span class=help-block';
        $this->wrappers['control']['errorcontainer'] = 'span class=help-block';

        if ($this->mode === self::MODE_VERTICAL) {
            $form->getElementPrototype()->addClass('form-horizontal');

            $this->wrappers['control']['container'] = 'div class=col-sm-9';
            $this->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        } elseif ($this->mode === self::MODE_HORIZONTAL) {
            $this->wrappers['control']['container'] = 'div class=col-sm-12';
            $this->wrappers['label']['container'] = 'div class="col-sm-12 control-label"';
        }
    }


    protected function prepareControls(Form $form)
    {
        foreach ($form->getControls() as $control) {
            if ($control instanceof Controls\Button) {
                if ($control->parent instanceof Container) {
                    $control->getControlPrototype()->addClass('btn btn-success');
                } elseif ($control->getOption('ignore')) {
                    $control->getControlPrototype()->addClass('btn btn-info');
                } else {
                    $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                    $usedPrimary = TRUE;
                }
            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }
    }

}