<?php

namespace movi\Forms;

use LeanMapper\Exception\Exception;
use LeanMapper\IMapper;
use LeanMapper\Relationship\HasMany;
use LeanMapper\Relationship\HasOne;
use movi\Application\UI\Form;
use movi\FormErrorException;
use movi\Model\Entity;
use movi\Model\Facade;
use Nette\Forms\Container;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

abstract class EntityFormFactory extends FormFactory
{

    /**
     * @var Facade
     */
    protected $facade;

    /**
     * @var IMapper
     */
    private $mapper;

    /**
     * @var Entity
     */
    protected $entity;


    public function __construct(Facade $facade, IMapper $mapper)
    {
        $this->facade = $facade;
        $this->mapper = $mapper;
    }


    public function create(Entity $entity = NULL)
    {
        if ($entity !== NULL) {
            $this->setEntity($entity);
        }

        if ($this->entity === NULL) {
            throw new InvalidStateException('Entity is not set.');
        }

        return parent::create();
    }


    public function processForm(Form $form)
    {
        $values = $form->values;

        $this->saveEntity($form, $this->entity, $values);
    }


    protected function saveEntity(Form $form, Entity $entity, $values)
    {
        $reflection = $entity->getReflection($this->mapper);
        $modified = $entity->getModifiedRowData();
        $hasMany = [];

        foreach ($reflection->getEntityProperties() as $property)
        {
            $name = $property->getName();
            $column = $property->getColumn();

            if (!array_key_exists($name, $values) || (isset($modified[$column]) || isset($modified[$column]) && $modified[$column] === $property->getDefaultValue())) {
                continue;
            }

            $value = $values[$name];

            if ($property->isBasicType() || $value instanceof \DateTime) {
                $entity->{$name} = $value;
            } elseif ($property->hasRelationship()) {
                $relationship = $property->getRelationship();

                if ($relationship instanceof HasOne && $value instanceof Entity) {
                    $entity->{$name} = $value;
                } elseif ($relationship instanceof HasMany) {
                    $hasMany[$name] = $value;
                }
            }
        }

        try {
            $this->facade->persist($entity);
        } catch (FormErrorException $e) {
            $form->addError($e->getMessage());
        }

        if (!empty($hasMany)) {
            foreach ($hasMany as $property => $entities)
            {
                $removeAll = 'removeAll' . Strings::firstUpper($property);
                $addTo = 'addTo' . Strings::firstUpper($property);

                $entity->{$removeAll}();
                $entity->{$addTo}($entities);
            }

            $this->facade->persist($entity);
        }
    }


    public function loadValues(Form $form)
    {
        if (!$this->entity->isDetached()) {
            foreach ($form->getControls() as $control)
            {
                try {
                    if (!isset($this->entity->{$control->getName()})) continue;

                    $value = $this->entity->{$control->getName()};

                    if ($control instanceof Container) {
                        $control->setDefaults($value);
                    } else {
                        $control->setDefaultValue($value);
                    }
                } catch (Exception $e) {

                }
            }
        }
    }


    /**
     * @param Entity $entity
     * @return $this
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;

        return $this;
    }


    /**
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }


    public function getFacade()
    {
        return $this->facade;
    }

} 