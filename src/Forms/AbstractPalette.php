<?php

namespace movi\Forms;

use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use movi\Application\UI\Form;

abstract class AbstractPalette implements IPalette
{

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var EventManager
     * @inject
     */
    public $evm;


    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }


    public function attach(Form $form)
    {
        $this->configure($form);

        if ($this->evm) {
            $this->evm->dispatchEvent(get_called_class() . '::onAttach', new EventArgsList([$form]));
        }
    }


    public function getFormFactory()
    {
        return $this->formFactory;
    }

}