<?php

namespace movi\Forms;

use Doctrine\Common\EventArgs;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use movi\Application\UI\Form;
use movi\Forms\Renderers\BootstrapFormRenderer;
use Nette\Localization\ITranslator;
use Nette\Object;

abstract class FormFactory extends Object
{

    /**
     * @var ITranslator
     * @inject
     */
    public $translator;

    /**
     * @var IPalette[]
     */
    protected $palettes;

    /**
     * @var EventManager
     * @inject
     */
    public $evm;


    public function create()
    {
        $form = new Form();
        $form->setRenderer(new BootstrapFormRenderer());
        $form->setTranslator($this->translator);

        if ($this->evm) {
            $this->evm->dispatchEvent(get_called_class() . '::onBeforeConfigure', new EventArgsList([$form, $this]));
        }

        // Initialize factory
        $this->preConfigure($form);
        $this->configure($form);
        $this->postConfigure($form);
        $this->loadValues($form);

        if ($this->evm) {
            $this->evm->dispatchEvent(get_called_class() . '::onConfigure', new EventArgsList([$form, $this]));
        }

        $form->onValidate[] = function(Form $form) {
            $this->checkForm($form);
        };

        $form->onError[] = function(Form $form) {
            $this->processErrors($form);
        };

        $form->onSuccess[] = function(Form $form) {
            $this->preprocessForm($form);

            $this->processForm($form);

            $this->postprocessForm($form);
        };

        return $form;
    }


    public function addPalette($key, IPalette $palette)
    {
        $palette->setFormFactory($this);

        $this->palettes[$key] = $palette;

        return $this;
    }


    protected function getPalette($key)
    {
        return $this->palettes[$key];
    }


    abstract protected function configure(Form $form);


    protected function preConfigure(Form $form)
    {

    }


    protected function postConfigure(Form $form)
    {

    }


    public function preprocessForm(Form $form)
    {

    }


    public function processForm(Form $form)
    {

    }


    public function postprocessForm(Form $form)
    {

    }


    public function checkForm(Form $form)
    {

    }


    public function processErrors(Form $form)
    {

    }


    public function loadValues(Form $form)
    {

    }

} 