<?php

namespace movi\Latte\Macros;

use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

class FilesMacros extends MacroSet
{

	public static function install(Compiler $compiler)
	{
		$set = new static($compiler);

		$set->addMacro('file', array($set, 'macroFile'));
		$set->addMacro('thumb', array($set, 'macroThumb'));
	}


	public function macroFile(MacroNode $node, PhpWriter $writer)
	{
		return $writer->write('echo $template->file(%node.args)');
	}


	public function macroThumb(MacroNode $node, PhpWriter $writer)
	{
		return $writer->write('echo $template->thumb(%node.array)');
	}

} 