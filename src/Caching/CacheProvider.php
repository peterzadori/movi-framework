<?php

namespace movi\Caching;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Object;

final class CacheProvider extends Object
{

	/** @var \Nette\Caching\IStorage */
	private $storage;


	public function __construct(IStorage $storage)
	{
		$this->storage = $storage;
	}


	/**
	 * @param null $namespace
	 * @return Cache
	 */
	public function create($namespace = NULL)
	{
		return new Cache($this->storage, $namespace);
	}

} 