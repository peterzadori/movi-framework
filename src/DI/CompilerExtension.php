<?php

namespace movi\DI;

abstract class CompilerExtension extends \Nette\DI\CompilerExtension
{

	protected function getSortedServices($tag)
	{
		$builder = $this->getContainerBuilder();
		$sorted = [];

		foreach(array_keys($builder->findByTag($tag)) as $service)
		{
			$definition = $builder->getDefinition($service);
			$tags = $definition->getTags();

			if (isset($tags['order'])) {
				$sorted[$tags['order']] = $definition;
			}
		}

		ksort($sorted);

		return $sorted;
	}

} 