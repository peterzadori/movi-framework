<?php

namespace movi\DI\Extensions;

use movi\DI\CompilerExtension;
use movi\InvalidArgumentException;
use movi\Model\Schema\SchemaTool;
use Nette\DI\ContainerBuilder;
use Nette\DI\Statement;
use Nette\PhpGenerator\ClassType;

final class moviExtension extends CompilerExtension
{

    const ROUTE_TAG = 'route',
        FILTER_TAG = 'orm.filter',
        WIDGET_TAG = 'widget',
        PRESENTER_TAG = 'presenter';

    private $defaults = [
        'database' => [],
        'entities' => [],
        'templating' => [
            'vars' => []
        ]
    ];

    private $defaultEntities = [
        'languages' => 'movi\Model\Entities\Language',
        'users' => 'movi\Model\Entities\User',
        'user_groups' => 'movi\Model\Entities\UserGroup',
        'files' => 'movi\Model\Entities\File'
    ];


    public function loadConfiguration()
    {
        $config = $this->getConfig($this->defaults);
        $builder = $this->getContainerBuilder();

        $this->initApplication($builder);

        $this->initDatabase($builder, $config);

        $this->initLocalization($builder);

        $this->initSecurity($builder);

        $this->initTemplating($builder, $config);

        $this->initRouting($builder, $config);

        $this->initCaching($builder);

        $this->initWidgets($builder);

        $this->initFiles($builder);

        $builder->addDefinition($this->prefix('flashes'))
            ->setImplement('movi\Components\Flashes\IFlashesFactory');

        $builder->addDefinition($this->prefix('head'))
            ->setImplement('movi\Components\Head\IHeadFactory');

        $builder->addDefinition($this->prefix('paramService'))
            ->setClass('movi\ParamService', array($builder->parameters));

        $builder->addDefinition($this->prefix('cacheCleanCommand'))
            ->setClass('movi\Commands\Cache\CleanCommand')
            ->addTag('kdyby.console.command');

        $builder->addDefinition($this->prefix('entitySerializationSubscriber'))
            ->setClass('movi\Subscribers\EntitySerializationSubscriber')
            ->addTag('kdyby.subscriber');

        $builder->addDefinition($this->prefix('languagesSubscriber'))
            ->setClass('movi\Subscribers\LanguagesSubscriber')
            ->addTag('kdyby.subscriber');
    }


    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        $this->registerRoutes($builder);

        $this->registerFilters($builder);

        $this->registerWidgets($builder);

        $this->registerPresenters($builder);
    }


    private function initApplication(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('presenters'))
            ->setClass('movi\Application\Presenters');

        $builder->getDefinition('application.presenterFactory')
            ->setClass('Nette\Application\IPresenterFactory')
            ->setFactory('movi\Application\PresenterFactory');
    }


    private function initDatabase(ContainerBuilder $builder, $config)
    {
        // Default entities
        $entities = array_merge($this->defaultEntities, $config['entities']);

        $connection = $builder->addDefinition($this->prefix('connection'))
            ->setClass('LeanMapper\Connection', [$config['database']]);

        $builder->addDefinition($this->prefix('entityMapping'))
            ->setClass('movi\Model\EntityMapping', [$entities]);

        $builder->addDefinition($this->prefix('mapper'))
            ->setClass('movi\Model\Mapper');

        $builder->addDefinition($this->prefix('entityFactory'))
            ->setClass('LeanMapper\DefaultEntityFactory');

        $builder->addDefinition($this->prefix('persister'))
            ->setClass('movi\Model\Persister');

        $builder->addDefinition('daoFactory')
            ->setClass('movi\Model\DAOFactory');

        $builder->addDefinition($this->prefix('translateFilter'))
            ->setClass('movi\Model\Filters\TranslateFilter')
            ->addTag(self::FILTER_TAG)
            ->addTag('name', 'translate')
            ->addTag('schema', 'e');

        $builder->addDefinition($this->prefix('queryFilter'))
            ->setClass('movi\Model\Filters\QueryFilter')
            ->addTag(self::FILTER_TAG)
            ->addTag('name', 'query');

        $panel = $builder->addDefinition($this->prefix('dibiPanel'))
            ->setClass('Dibi\Bridges\Tracy\Panel');

        $connection->addSetup(array($panel, 'register'), array($connection));

        $builder->addDefinition($this->prefix('filesFacade'))
            ->setClass('movi\Model\Facades\FilesFacade', array(
                new Statement('@daoFactory::getDao', array('movi\Model\Entities\File'))
            ));

        // Schema
        $builder->addDefinition($this->prefix('schemaTool'))
            ->setClass('movi\Model\Schema\SchemaTool');

        $builder->addDefinition($this->prefix('entitiesSchemaFactory'))
            ->setClass('movi\Model\Schema\Schemas\EntitiesSchemaFactory')
            ->addTag(SchemaTool::SCHEMA_FACTORY_TAG);

        $builder->addDefinition($this->prefix('schemaUpdateCommand'))
            ->setClass('movi\Commands\Schema\UpdateCommand')
            ->addTag('kdyby.console.command');
    }


    private function registerFilters(ContainerBuilder $builder)
    {
        $connection = $builder->getDefinition($this->prefix('connection'));

        foreach ($builder->findByTag(self::FILTER_TAG) as $name => $service)
        {
            $service = $builder->getDefinition($name);
            if ($service->getTag('name') === NULL) throw new InvalidArgumentException;

            $name = $service->getTag('name');
            $callback = $service->getTag('callback') ? $service->getTag('callback') : 'filter';
            $schema = $service->getTag('schema') ? $service->getTag('schema') : NULL;

            $connection->addSetup('registerFilter', [
                $name, [$service, $callback], $schema
            ]);
        }
    }


    private function initLocalization(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('languagesFacade'))
            ->setClass('movi\Model\Facades\LanguagesFacade', [new Statement('@daoFactory::getDao', ['movi\Model\Entities\Language'])]);

        $builder->addDefinition($this->prefix('languages'))
            ->setClass('movi\Localization\Languages');

        $builder->addDefinition($this->prefix('translator'))
            ->setClass('movi\Localization\Translator', array($builder->expand('%localeDir%')));
    }


    public function initSecurity(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('usersFacade'))
            ->setClass('movi\Model\Facades\UsersFacade', [new Statement('@daoFactory::getDao', ['movi\Model\Entities\User'])]);

        $builder->addDefinition($this->prefix('userGroupsFacade'))
            ->setClass('movi\Model\Facades\UserGroupsFacade', [new Statement('@daoFactory::getDao', ['movi\Model\Entities\UserGroup'])]);

        $builder->getDefinition('nette.userStorage')
            ->setFactory('movi\Security\UserStorage');

        $builder->addDefinition($this->prefix('authenticator'))
            ->setClass('movi\Security\Authenticator');

        $builder->addDefinition($this->prefix('authorizator'))
            ->setClass('movi\Security\Authorizator');

        $builder->getDefinition('user')
            ->setClass('movi\Security\User')
            ->setFactory('movi\Security\User');

    }


    private function initTemplating(ContainerBuilder $builder, $config)
    {
        $builder->getDefinition('nette.templateFactory')
            ->setClass('movi\Application\UI\TemplateFactory')
            ->setFactory('movi\Application\UI\TemplateFactory')
            ->addSetup('setTranslator');

        $latte = $builder->getDefinition('nette.latteFactory');
        $latte->addSetup('?->onCompile[] = function($engine) { movi\Latte\Macros\FilesMacros::install($engine->getCompiler()); }', array('@self'));
    }


    private function initRouting(ContainerBuilder $builder, $config)
    {

    }


    private function registerRoutes(ContainerBuilder $builder)
    {
        $router = $builder->getDefinition('router');
        $routes = $this->getSortedServices(self::ROUTE_TAG);

        foreach ($routes as $route)
        {
            $route->setAutowired(false);

            $router->addSetup('$service[] = ?', [$route]);
        }
    }


    public function initCaching(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('cacheProvider'))
            ->setClass('movi\Caching\CacheProvider');
    }


    private function initWidgets(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('widgetManager'))
            ->setClass('movi\Components\Widgets\WidgetManager');
    }


    public function initFiles(ContainerBuilder $builder)
    {
        $builder->addDefinition($this->prefix('metadataReader'))
            ->setClass('movi\Files\Metadata\Reader', array($builder->expand('%metadataDir%')));

        $builder->addDefinition($this->prefix('metadataWriter'))
            ->setClass('movi\Files\Metadata\Writer', array($builder->expand('%metadataDir%')));

        $builder->addDefinition($this->prefix('localFiles'))
            ->setClass('movi\Files\Storages\LocalFiles');

        $builder->addDefinition($this->prefix('fileStorage'))
            ->setClass('movi\Files\Storages\Local', array($builder->expand('%storageDir%')));

        $builder->addDefinition($this->prefix('filesManager'))
            ->setClass('movi\Files\FilesManager');

        $builder->addDefinition($this->prefix('fileLinker'))
            ->setClass('movi\Files\Utils\FileLinker');

        $builder->addDefinition($this->prefix('thumbnailCreator'))
            ->setClass('movi\Files\Utils\ThumbnailCreator');

        $builder->addDefinition($this->prefix('facadeSubscriber'))
            ->setClass('movi\Subscribers\FacadeSubscriber')
            ->addTag('kdyby.subscriber');
    }


    private function registerWidgets(ContainerBuilder $builder)
    {
        $widgetManager = $builder->getDefinition($this->prefix('widgetManager'));

        foreach ($builder->findByTag(self::WIDGET_TAG) as $name => $service)
        {
            $service = $builder->getDefinition($name);
            if (!$service->getTag('name')) throw new InvalidArgumentException;

            $name = $service->getTag('name');
            $widgetManager->addSetup('addWidget', array($name, $service));
        }
    }


    private function registerPresenters(ContainerBuilder $builder)
    {
        $presenters = $builder->getDefinition($this->prefix('presenters'));

        foreach ($this->getSortedServices(self::PRESENTER_TAG) as $name => $service)
        {
            $presenters->addSetup('addPresenter', array($service, $service->getTags()));
        }
    }

} 