<?php

namespace movi\DI\Extensions;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IAssetsProvider;
use movi\Packages\Providers\IMappingProvider;
use Nette\DI\Statement;
use Nette\Utils\Strings;

class PackagesExtension extends CompilerExtension
{

	const INSTALLER_TAG = 'package.installer';


	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

        $presenterFactory = $builder->getDefinition('application.presenterFactory');

        foreach ($this->compiler->getExtensions() as $extension)
        {
            if (!$extension instanceof IPackage) continue;

            if ($extension instanceof IMappingProvider) {
                foreach ($extension->getMappings() as $module => $mapping)
                {
                    $presenterFactory->addSetup('setMapping', array(array($module => $mapping)));
                }
            }
        }
	}

} 