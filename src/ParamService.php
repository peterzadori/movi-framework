<?php

namespace movi;

use Nette\Object;

final class ParamService extends Object
{

	/**
	 * @var array
	 */
	private $params = [];


	public function __construct(array $params)
	{
		$this->params = $params;
	}


	/**
	 * @param $key
	 * @return mixed|null
	 */
	public function getParam($key)
	{
		return isset($this->params[$key]) ? $this->params[$key] : NULL;
	}

} 