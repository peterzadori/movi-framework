<?php

namespace movi\Model;

use LeanMapper\Connection;
use LeanMapper\IEntityFactory;
use LeanMapper\IMapper;
use Nette\Object;

class DAOFactory extends Object
{

	/**
	 * @var Connection
	 */
	private $connection;

	/**
	 * @var IMapper
	 */
	private $mapper;

	/**
	 * @var IEntityFactory
	 */
	private $entityFactory;

	/**
	 * @var Persister
	 */
	private $persister;

	private $registry = [];


	public function __construct(Connection $connectiom, IMapper $mapper, IEntityFactory $entityFactory, Persister $persister)
	{
		$this->connection = $connectiom;
		$this->mapper = $mapper;
		$this->entityFactory = $entityFactory;
		$this->persister = $persister;
	}


	/**
	 * @param $entity
	 * @return DAORepository
	 */
	public function getDao($entity)
	{
		$table = $this->mapper->getTable($entity);

		if (isset($this->registry[$table])) {
			return $this->registry[$table];
		}

		$repository = new DAORepository($table, $this->connection, $this->mapper, $this->entityFactory, $this->persister);

		$this->registry[$table] = $repository;

		return $repository;
	}

} 