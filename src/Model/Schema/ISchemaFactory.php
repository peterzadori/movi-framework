<?php

namespace movi\Model\Schema;

use Doctrine\DBAL\Schema\Schema;

interface ISchemaFactory
{

    public function createSchema(Schema $schema);

}