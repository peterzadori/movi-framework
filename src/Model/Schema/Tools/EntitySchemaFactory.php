<?php

namespace movi\Model\Schema\Tools;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Types\Type;
use LeanMapper\Reflection\Property;
use LeanMapper\Relationship\HasMany;
use LeanMapper\Relationship\HasOne;
use movi\Model\Entities\TranslatableEntity;
use movi\Model\Entity;
use movi\Model\Mapper;

class EntitySchemaFactory
{

    /**
     * @var Mapper
     */
    private $mapper;


    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Creates schema for provided entity
     *
     * @param Schema $schema
     * @param Entity $entity
     * @param string|NULL $tableName
     */
    public function createEntitySchema(Schema $schema, Entity $entity, $tableName = NULL)
    {
        $reflection = $entity->getReflection($this->mapper);
        $properties = $reflection->getEntityProperties();
        $tableName = $tableName ? $tableName : $this->mapper->getTable(get_class($entity));
        $translationsTableName = $this->mapper->getTranslationsTable($tableName);

        if (count($properties) === 0) return;

        if ($entity instanceof TranslatableEntity) {
            if ($schema->hasTable($translationsTableName)) {
                $translationsTable = $schema->getTable($translationsTableName);
            } else {
                $translationsTable = $schema->createTable($translationsTableName);
                $translationColumn = $translationsTable->addColumn($this->mapper->getRelationshipColumn($tableName, $tableName), 'integer');

                $translationsTable->addForeignKeyConstraint($tableName, [$translationColumn->getName()], [$this->mapper->getPrimaryKey($tableName)], array('onDelete' => 'CASCADE'));
            }
        }

        $table = $schema->hasTable($tableName) ? $schema->getTable($tableName) : $schema->createTable($tableName);
        $_table = $table;

        foreach ($properties as $property) {
            if ($property->hasCustomFlag('translate') && $entity instanceof TranslatableEntity) {
                $table = $translationsTable;
            }

            if ($property->hasCustomFlag('ignore')) continue;

            if (!$property->hasRelationship() && !$table->hasColumn($property->getColumn())) {
                $type = $this->getType($property);

                if ($type === NULL) {
                    continue;
                }

                /** @var \Doctrine\DBAL\Schema\Column $column */
                $column = $table->addColumn($property->getColumn(), $type);

                if ($property->hasCustomFlag('autoincrement')) {
                    $column->setAutoincrement(true);
                    $table->setPrimaryKey([$property->getColumn()]);
                }

                if ($property->hasCustomFlag('unique')) {
                    $table->addUniqueIndex([$column->getName()]);
                }

                if ($property->containsEnumeration()) {
                    $column->getType()->setEnumeration($property->getEnumValues());
                }

                if ($property->hasCustomFlag('size')) {
                    $column->setLength($property->getCustomFlagValue('size'));
                }

                if ($column->getType()->getName() === Type::FLOAT) {
                    if ($property->hasCustomFlag('scale')) {
                        $column->setScale($property->getCustomFlagValue('scale'));
                    }

                    if ($property->hasCustomFlag('precision')) {
                        $column->setPrecision($property->getCustomFlagValue('precision'));
                    }
                }
            } else {
                $relationship = $property->getRelationship();

                if ($relationship instanceof HasMany && !$schema->hasTable($relationship->getRelationshipTable())) {
                    $relationshipTable = $schema->createTable($relationship->getRelationshipTable());

                    $relationshipTable->addColumn($relationship->getColumnReferencingSourceTable(), 'integer');
                    $relationshipTable->addColumn($relationship->getColumnReferencingTargetTable(), 'integer');

                    $relationshipTable->addForeignKeyConstraint($tableName, [$relationship->getColumnReferencingSourceTable()], [$this->mapper->getPrimaryKey($relationship->getRelationshipTable())], array('onDelete' => 'CASCADE'));
                    $relationshipTable->addForeignKeyConstraint($relationship->getTargetTable(), [$relationship->getColumnReferencingTargetTable()], [$this->mapper->getPrimaryKey($relationship->getRelationshipTable())], array('onDelete' => 'CASCADE'));
                } elseif ($relationship instanceof HasOne && !$table->hasColumn($relationship->getColumnReferencingTargetTable())) {
                    $column = $table->addColumn($relationship->getColumnReferencingTargetTable(), 'integer');

                    if ($property->hasCustomFlag('onDelete')) {
                        $cascade = $property->getCustomFlagValue('onDelete');
                    } else {
                        $cascade = $property->isNullable() ? 'SET NULL' : 'CASCADE';
                    }

                    $table->addForeignKeyConstraint($relationship->getTargetTable(), [$column->getName()], [$this->mapper->getPrimaryKey($relationship->getTargetTable())], array('onDelete' => $cascade));
                }
            }

            if (isset($column)) {
                if ($property->isNullable()) {
                    $column->setNotnull(false);
                }

                if ($property->hasDefaultValue()) {
                    $column->setDefault($property->getDefaultValue());
                }
            }

            $table = $_table;
        }
    }


    /**
     * @param Property $property
     * @return null|string
     * @throws \LeanMapper\Exception\InvalidArgumentException
     */
    private function getType(Property $property)
    {
        $type = NULL;

        if ($property->isBasicType()) {
            $type = $property->getType();

            switch ($type)
            {
                case 'string':
                    if (!$property->hasCustomFlag('size')) {
                        $type = 'text';
                    }
                    break;
            }

            if ($property->containsEnumeration()) {
                $type = 'enum';
            }
        } else {
            $class = new \ReflectionClass($property->getType());
            $class = $class->newInstance();

            if ($class instanceof \DateTime) {
                if ($property->hasCustomFlag('format')) {
                    $type = $property->getCustomFlagValue('format');
                } else {
                    $type = 'datetime';
                }
            }
        }

        return $type;
    }

}