<?php

namespace movi\Model\Schema\Schemas;

use Doctrine\DBAL\Schema\Schema;
use movi\Model\Mapper;
use movi\Model\Schema\ISchemaFactory;
use movi\Model\Schema\Tools\EntitySchemaFactory;

class EntitiesSchemaFactory implements ISchemaFactory
{

    /**
     * @var Mapper
     */
    private $mapper;


    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function createSchema(Schema $schema)
    {
        $entities = $this->mapper->getEntities();
        $entitySchemaFactory = new EntitySchemaFactory($this->mapper);

        foreach ($entities as $entityClass)
        {
            $entity = new $entityClass;

            $entitySchemaFactory->createEntitySchema($schema, $entity);
        }
    }

}