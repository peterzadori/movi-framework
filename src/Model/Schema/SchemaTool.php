<?php

namespace movi\Model\Schema;

use Doctrine\DBAL\Driver\PDOMySql\Driver;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\DBAL\Schema\Comparator;
use Doctrine\DBAL\Schema\MySqlSchemaManager;
use Doctrine\DBAL\Schema\Schema;
use LeanMapper\Connection;
use Nette\DI\Container;
use Nette\Object;
use Doctrine\DBAL\Connection as DBALConnection;

class SchemaTool extends Object
{

    const SCHEMA_FACTORY_TAG = 'schema.factory';

    /**
     * @var Container
     */
    private $serviceLocator;

    /**
     * @var Connection
     */
    private $connection;

    public $onExecuteQuery;


    public function __construct(Container $container, Connection $connection)
    {
        $this->serviceLocator = $container;
        $this->connection = $connection;
    }


    /**
     * Updates current database schema
     */
    public function updateSchema()
    {
        $comparator = new Comparator();
        $currentSchema = $this->getCurrentSchema();
        $desiredSchema = $this->getDesiredSchema();

        $queries = $comparator->compare($currentSchema, $desiredSchema)->toSql($this->getPlatform());

        foreach ($queries as $query)
        {
            $this->connection->query($query);

            $this->onExecuteQuery($query);
        }
    }


    /**
     * Returns current database schema
     *
     * @return Schema
     */
    public function getCurrentSchema()
    {
        return $this->getSchemaManager()->createSchema();
    }


    /**
     * Returns desired database schema
     *
     * @return Schema
     */
    public function getDesiredSchema()
    {
        $schema = new Schema();

        foreach (array_keys($this->serviceLocator->findByTag(self::SCHEMA_FACTORY_TAG)) as $serviceName)
        {
            /** @var ISchemaFactory $schemaFactory */
            $schemaFactory = $this->serviceLocator->getService($serviceName);

            $schemaFactory->createSchema($schema);
        }

        return $schema;
    }


    /**
     * @return DBALConnection
     */
    private function getDbalConnection()
    {
        return new DBALConnection(array(
            'dbname' => $this->connection->getConfig('database'),
            'user' => $this->connection->getConfig('username'),
            'password' => $this->connection->getConfig('password'),
            'host' => $this->connection->getConfig('host'),
            'port' => $this->connection->getConfig('port')
        ), new Driver());
    }


    /**
     * @return MySqlSchemaManager
     */
    public function getSchemaManager()
    {
        return new MySqlSchemaManager($this->getDbalConnection(), $this->getPlatform());
    }


    /**
     * @return MySqlPlatform
     */
    public function getPlatform()
    {
        return new MySqlPlatform();
    }

}