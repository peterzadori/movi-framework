<?php

namespace movi\Model;

use LeanMapper\Connection;
use LeanMapper\IEntityFactory;
use LeanMapper\IMapper;

class DAORepository extends Repository
{

	public function __construct($table, Connection $connection, IMapper $mapper, IEntityFactory $entityFactory, Persister $persister)
	{
		parent::__construct($connection, $mapper, $entityFactory, $persister);

		$this->table = $table;
	}

} 