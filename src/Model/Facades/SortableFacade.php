<?php

namespace movi\Model\Facades;

use movi\Model\Entity;
use movi\Model\Query;

trait SortableFacade
{

    public function getSortedRows()
    {
        $query = new Query();
        $query->orderBy('@order ASC');

        return $this->findAll($query);
    }


    public function moveUp(Entity $entity)
    {
        $query = (new Query())->where('@order = %i', $entity->order - 1);
        $previous = $this->find($query);

        if ($previous) {
            $previous->order++;
            $this->persist($previous);

            $entity->order--;
            $this->persist($entity);
        }
    }


    public function moveDown(Entity $entity)
    {
        $query = (new Query())->where('@order = %i', $entity->order + 1);
        $next = $this->find($query);

        if ($next) {
            $next->order--;
            $this->persist($next);

            $entity->order++;
            $this->persist($entity);
        }
    }


    public function delete(Entity $entity)
    {
        $query = (new Query())->where('@order > %i', $entity->order);
        foreach ($this->findAll($query) as $row)
        {
            $row->order--;
            $this->persist($row);
        }

        parent::delete($entity);
    }

}