<?php

namespace movi\Model\Facades;

use movi\Model\Entities\Language;
use movi\Model\Facade;
use movi\Model\Query;

class LanguagesFacade extends Facade
{

    /**
     * @return Language[]
     */
    public function findActive()
    {
        try {
            $query = (new Query())->where('@active', true);
            $rows = $this->findAll($query);
            $languages = [];

            foreach($rows as $row)
            {
                $languages[$row->code] = $row;
            }

            return $languages;
        } catch (\DibiException $e) {
            return [];
        }
    }

} 