<?php

namespace movi\Model\Facades;

use movi\Files\File;
use movi\Model\Facade;
use movi\Model\Entities\File as FileEntity;
use movi\Model\Query;

class FilesFacade extends Facade
{

	/**
	 * @return array
	 */
	public function getFiles()
	{
		$rows = $this->findAll();
		$files = [];

		/** @var \movi\Model\Entities\File $file */
		foreach ($rows as $file)
		{
			$files[$file->key] = $file->file;
		}

		return $files;
	}


	/**
	 * @param File $file
	 * @return File
	 */
	public function create(File $file)
	{
		$entity = new FileEntity(array(
			'key' => $file->getKey(),
			'file' => $file->getFile()
		));

		$this->persist($entity);

		return $file;
	}


	/**
	 * @param File $file
	 */
	public function remove(File $file)
	{
		$query = new Query();
		$query->where('@key', $file->getKey());

		$entity = $this->find($query);

		if ($entity) {
			$this->delete($entity);
		}
	}

} 