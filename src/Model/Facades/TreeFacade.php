<?php

namespace movi\Model\Facades;

use movi\Model\Query;
use movi\Model\TranslationsFacade;

abstract class TreeFacade extends TranslationsFacade
{

	/**
	 * @param Query $query
	 * @return array|\DibiRow|FALSE|mixed
	 */
	public function findAll(Query $query = NULL)
	{
		if ($query === NULL) {
			$query = new Query;
		}

		$query->orderBy('@order');

		return parent::findAll($query);
	}

} 