<?php

namespace movi\Model\Facades;

use movi\FormErrorException;
use movi\Model\Entities\User;
use movi\Model\Entity;
use movi\Model\Facade;
use movi\Model\Query;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;

class UsersFacade extends Facade
{

	public function persist(Entity $entity)
	{
		if ($entity->isDetached()) {
			return $this->create($entity);
		} else {
			return parent::persist($entity);
		}
	}


	public function create(User $user)
	{
		if ($this->checkEmail($user->email)) {
			throw new FormErrorException('E-mail sa už používa.');
		}

		return parent::persist($user);
	}


	public function login($email, $password)
	{
		$query = new Query();
		$query->where('@email = %s', $email);

		$account = $this->find($query);

		if (!$account) {
			throw new AuthenticationException();
		}

		if (!Passwords::verify($password, $account->password)) {
			throw new AuthenticationException();
		}

		return $account;
	}


	public function checkEmail($email)
	{
		$query = new Query();
		$query->where('@email = %s', $email);

		return $this->find($query);
	}

} 