<?php

namespace movi\Model;

use LeanMapper\Connection;
use LeanMapper\IEntityFactory;
use LeanMapper\IMapper;
use movi\Model\Entities\TranslatableEntity;

abstract class Repository extends \LeanMapper\Repository
{

	/** @var \movi\Model\Persister */
	protected $persister;


	public function __construct(Connection $connection, IMapper $mapper, IEntityFactory $entityFactory, Persister $persister)
	{
		parent::__construct($connection, $mapper, $entityFactory);

		$this->persister = $persister;
	}


	/**
	 * @param Query $query
	 * @return array|bool|int|mixed
	 * @throws \LeanMapperQuery\Exception\InvalidArgumentException
	 */
	public function find(Query $query)
	{
		$statement = $this->createFluent();
		$query->applyQuery($statement, $this->mapper);

		switch ($query->getMode())
		{
			case $query::SINGLE_MODE:
				$row = $statement->fetch();

				if (!$row) {
					return false;
				}

				return $this->createEntity($row);
				break;

			case $query::COLLECTION_MODE:
				$rows = $statement->fetchAll();

				return $this->createEntities($rows);
				break;

			case $query::COUNTER_MODE:
				return $statement->count();
				break;
		}
	}


	protected function insertIntoDatabase(\LeanMapper\Entity $entity)
	{
		return $this->persister->insert($this->table, $entity);
	}


	public function updateInDatabase(\LeanMapper\Entity $entity)
	{
		return $this->persister->update($this->table, $entity);
	}


	public function getTranslations(TranslatableEntity $entity)
	{
		$table = $this->getTable();
		$languageColumn = 'language_id';
		$translationsTable = $this->mapper->getTranslationsTable($table);
		$translationsColumn = $this->mapper->getTranslationsColumn($table);
		$primaryKey = $this->mapper->getPrimaryKey($table);
		$idField = $this->mapper->getEntityField($table, $primaryKey);

		return $this->connection->select('*')
			->from($translationsTable)
			->where('%n = %i', $translationsColumn, $entity->$idField)
			->fetchAssoc($languageColumn);
	}


	/**
	 * @return Connection
	 */
	public function getConnection()
	{
		return $this->connection;
	}

} 