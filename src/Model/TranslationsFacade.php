<?php

namespace movi\Model;

use movi\Model\Entities\TranslatableEntity;

abstract class TranslationsFacade extends Facade
{

	/**
	 * Returns translations for entity
	 *
	 * @param TranslatableEntity $entity
	 * @return array
	 */
	public function getTranslations(TranslatableEntity $entity)
	{
		return $this->repository->getTranslations($entity);
	}

} 