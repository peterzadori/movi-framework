<?php

namespace movi\Model;

use Doctrine\Common\Util\Inflector;
use LeanMapper\Caller;
use LeanMapper\DefaultMapper;
use LeanMapper\ImplicitFilters;
use LeanMapper\Row;
use movi\Model\Entities\ICustomFilterEntity;
use movi\Model\Entities\TranslatableEntity;
use Nette\Utils\Strings;

class Mapper extends DefaultMapper
{

	/**
	 * @var EntityMapping
	 */
	private $entityMapping;


	public function __construct(EntityMapping $entityMapping)
	{
		$this->entityMapping = $entityMapping;
	}


	public function getTable($entityClass)
	{
		$entityClass = $this->checkEntityClass($entityClass);

		return $this->entityMapping->getTable($entityClass);
	}


	public function getEntityClass($table, Row $row = NULL)
	{
		if (isset($row->entity) && !empty($row->entity)) {
			return $row->entity;
		}

		return $this->entityMapping->getEntityClass($table);
	}


	public function getEntities()
	{
		return array_values($this->entityMapping->getEntities());
	}


	public function getRelationshipColumn($sourceTable, $targetTable)
	{
		return Inflector::singularize($targetTable) . '_' . $this->getPrimaryKey($targetTable);
	}


	public function getTranslationsTable($table)
	{
		return Inflector::singularize($table) . '_translations';
	}


	public function getTranslationsColumn($table)
	{
		return Inflector::singularize($table) . '_id';
	}


	public function getImplicitFilters($entityClass, Caller $caller = null)
	{
		$entity = new $entityClass;

		$filters = array();
		$filters[] = 'query';

		if ($entity instanceof TranslatableEntity) {
			$filters[] = 'translate';
		}

		if ($entity instanceof ICustomFilterEntity) {
			foreach ($entity->getFilters() as $filter)
			{
				$filters[] = $filter;
			}
		}

		return new ImplicitFilters($filters);
	}


	public static function checkEntityClass($entityClass)
	{
		if (!Strings::startsWith($entityClass, '\\')) {
			$entityClass = '\\' . $entityClass;
		}

		return $entityClass;
	}

} 