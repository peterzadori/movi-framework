<?php

namespace movi\Model;

use LeanMapper\Connection;
use LeanMapper\IMapper;
use movi\Localization\Translator;
use movi\Model\Entities\TranslatableEntity;
use Nette\Object;

final class Persister extends Object
{

	/**
	 * @var Connection
	 */
	private $connection;

	/**
	 * @var IMapper
	 */
	private $mapper;

	/**
	 * @var Translator
	 */
	private $translator;


	public function __construct(Connection $connection, IMapper $mapper, Translator $translator)
	{
		$this->connection = $connection;
		$this->mapper = $mapper;
		$this->translator = $translator;
	}


	public function insert($table, Entity $entity)
	{
		$primaryKey = $this->mapper->getPrimaryKey($table);
		$values = $entity->getModifiedRowData();

		if ($entity instanceof TranslatableEntity) {
			$values = array_diff_key($entity->getModifiedRowData(), $entity->getTranslatableColumns());
		}

		$this->connection->query(
			'INSERT INTO %n %v', $table, $values
		);

		$id = isset($values[$primaryKey]) ? $values[$primaryKey] : $this->connection->getInsertId();

		if ($entity instanceof TranslatableEntity) {
			$this->insertTranslations($table, $id, $entity);
		}

		return $id;
	}


	public function update($table, Entity $entity)
	{
		$primaryKey = $this->mapper->getPrimaryKey($table);
		$idField = $this->mapper->getEntityField($table, $primaryKey);
		$values = $entity->getModifiedRowData();
		$result = NULL;

		if ($entity instanceof TranslatableEntity) {
			$values = array_diff_key($entity->getModifiedRowData(), $entity->getTranslatableColumns());
		}

		if (!empty($values)) {
			$result = $this->connection->query(
				'UPDATE %n SET %a WHERE %n = ?', $table, $values, $primaryKey, $entity->$idField
			);
		}

		if ($entity instanceof TranslatableEntity) {
			$this->insertTranslations($table, $entity->$idField, $entity);
		}

		return $result;
	}


	private function insertTranslations($table, $id, TranslatableEntity $entity)
	{
		$languageColumn = 'language_id';
		$translationsTable = $this->mapper->getTranslationsTable($table);
		$translationsViaColumn = $this->mapper->getTranslationsColumn($table);

		// Translation
		$translation = array_intersect_key($entity->getModifiedRowData(), $entity->getTranslatableColumns());
		$translation[$translationsViaColumn] = $id;

		if ($entity->isDetached()) {
			if (!isset($translation[$languageColumn])) {
				$translation[$languageColumn] = $this->translator->getLanguage()->id;
			}

			$this->connection->query(
				'INSERT INTO %n %v', $translationsTable, $translation
			);
		} else {
			if (isset($translation[$languageColumn])) {
				$row = $this->connection->select('*')
					->from($translationsTable)
					->where('%n = %i', $languageColumn, $translation[$languageColumn])
					->where('%n = %i', $translationsViaColumn, $id)
					->fetchSingle();

				if (!$row) {
					$this->connection->query(
						'INSERT INTO %n %v', $translationsTable, $translation
					);
				} else {
					$this->connection->query(
						'UPDATE %n SET %a WHERE %n = ? AND %n = %s',
						$translationsTable, $translation, $translationsViaColumn, $id, $languageColumn, $translation[$languageColumn]
					);
				}
			}
		}
	}

} 