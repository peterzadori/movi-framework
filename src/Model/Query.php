<?php

namespace movi\Model;

class Query extends \LeanMapperQuery\Query
{

	const SINGLE_MODE = 'single',
		COLLECTION_MODE = 'collection',
		COUNTER_MODE = 'counter';


	private $mode = self::SINGLE_MODE;


	/**
	 * @param $mode
	 * @return $this
	 */
	public function switchMode($mode)
	{
		$this->mode = $mode;

		return $this;
	}


	/**
	 * @return string
	 */
	public function getMode()
	{
		return $this->mode;
	}


	/**
	 * @param $cond
	 * @throws \LeanMapperQuery\Exception\InvalidStateException
	 */
	protected function commandHaving($cond)
	{
		$this->getFluent()->having(array(func_get_args()));
	}

} 