<?php

namespace movi\Model\Entities;
use movi\Security\IUserEntity;

/**
 * Class User
 * @package movi\Model\Entities
 *
 * @property string $name m:size(64)
 * @property string $email m:size(128)
 * @property string $password m:size(64)
 * @property bool $active = true
 * @property bool $superadmin
 *
 * @property string $role
 * @property UserGroup|NULL $group m:hasOne
 */
class User extends IdentifiedEntity implements IUserEntity
{

    const SUPERADMIN = 'superadmin';


	public function getId()
	{
		return $this->row->id;
	}


	public function getRoles()
	{
		return array($this->superadmin ? self::SUPERADMIN : NULL, $this->group);
	}

} 