<?php

namespace movi\Model\Entities;

use Nette\Utils\Json;

/**
 * Class UserGroup
 * @package movi\Model\Entities
 *
 * @property string $name m:size(32)
 * @property string $permissions
 */
class UserGroup extends IdentifiedEntity
{

    public function setPermissions($permissions)
    {
        $this->row->permissions = Json::encode($permissions);
    }


    public function getPermissions()
    {
        return Json::decode($this->row->permissions, Json::FORCE_ARRAY);
    }

}