<?php

namespace movi\Model\Entities;

use movi\Model\Entity;

/**
 * @property-read int $id m:autoincrement
 */
abstract class IdentifiedEntity extends Entity
{

} 