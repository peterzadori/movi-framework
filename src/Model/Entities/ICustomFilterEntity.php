<?php

namespace movi\Model\Entities;

interface ICustomFilterEntity
{

	/**
	 * @return array
	 */
	public function getFilters();

} 