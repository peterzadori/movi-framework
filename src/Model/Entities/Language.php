<?php

namespace movi\Model\Entities;

/**
 * Class Language
 * @package movi\Model\Entities
 *
 * @property string $name m:size(64)
 * @property string $code m:size(2)
 * @property bool $active = true
 * @property bool $default = true
 */
class Language extends IdentifiedEntity
{

} 