<?php

namespace movi\Model\Entities;

/**
 * Class File
 * @package movi\Model\Entities
 *
 * @property string $key
 * @property string $file m:size(128)
 *
 */
class File extends IdentifiedEntity
{

} 