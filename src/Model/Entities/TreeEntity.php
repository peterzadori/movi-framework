<?php

namespace movi\Model\Entities;

use Nette\Utils\Strings;

/**
 * Class TreeEntity
 * @package movi\Model\Entities
 *
 * @property string $path m:translate
 * @property string $name m:translate
 *
 * @property int $order = 1
 */
abstract class TreeEntity extends TranslatableEntity implements \JsonSerializable
{

	public function setName($name)
	{
		$this->row->name = $name;
		$this->path = Strings::webalize($name);
	}


    public function getParentId()
    {
        return $this->row->parent_id;
    }


	public function jsonSerialize()
	{
		return $this->id;
	}

} 