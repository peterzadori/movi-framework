<?php

namespace movi\Model\Entities;

use movi\Model\Entities\Language as LanguageEntity;

/**
 * Class TranslatableEntity
 * @package movi\Model\Entities
 *
 * @property LanguageEntity $language m:hasOne m:translate
 */
abstract class TranslatableEntity extends IdentifiedEntity
{

	const FLAG_TRANSLATE = 'translate';


	public function getTranslatableColumns()
	{
		$columns = [];
		$reflection = $this->getCurrentReflection();

		foreach ($reflection->getEntityProperties() as $property)
		{
			if ($property->hasCustomFlag(self::FLAG_TRANSLATE)) {
				$columns[$property->getColumn()] = true;
			}
		}

		return $columns;
	}

} 