<?php

namespace movi\Model\Filters;

use LeanMapper\Fluent;
use LeanMapper\IMapper;
use movi\Model\Query;
use Nette\Utils\Callback;

class QueryFilter
{

	/**
	 * @var IMapper
	 */
	private $mapper;


	public function __construct(IMapper $mapper)
	{
		$this->mapper = $mapper;
	}


	public function filter(Fluent $fluent, $callback = NULL)
	{
		if ($callback) {
			$query = new Query();
			Callback::invokeArgs($callback, array($query));
			$query->applyQuery($fluent, $this->mapper);
		}
	}

}