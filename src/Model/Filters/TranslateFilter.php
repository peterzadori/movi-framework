<?php

namespace movi\Model\Filters;

use LeanMapper\Fluent;
use LeanMapper\Reflection\Property;
use movi\Localization\Translator;
use movi\Model\Mapper;

class TranslateFilter
{

	/**
	 * @var Mapper
	 */
	private $mapper;

	/**
	 * @var Translator
	 */
	private $translator;


	public function __construct(Mapper $mapper, Translator $translator)
	{
		$this->mapper = $mapper;
		$this->translator = $translator;
	}


	/**
	 * @param Fluent $fluent
	 * @param null $table
	 */
	public function filter(Fluent $fluent, $table = NULL)
	{
		if ($table instanceof Property) {
			if ($table->hasRelationship()) {
				$relationship = $table->getRelationship();
				$table = $relationship->getTargetTable();

				$this->modifyStatement($fluent, $table);
			}
		} else {
			$table = $fluent->_export('SELECT');
			$table = $table[2];

			$this->modifyStatement($fluent, $table);
		}
	}


	/**
	 * @param Fluent $fluent
	 * @param $table
	 */
	private function modifyStatement(Fluent $fluent, $table)
	{
		$translationsTable = $this->mapper->getTranslationsTable($table);
		$defaultLanguage = $this->translator->getDefaultLanguage();

		if ($this->translator->getLanguage() !== NULL) {
			$fluent->select('%n.*', $translationsTable);
			$fluent->leftJoin($translationsTable);
			$fluent->on('%n.%n = %n.%n AND (%n.language_id = %i OR %n.language_id = %i)',
				$translationsTable, $this->mapper->getTranslationsColumn($table), $table, 'id',
				$translationsTable, $this->translator->getLanguage()->id,
				$translationsTable, $defaultLanguage->id);
            $fluent->orderBy('%n.language_id', $translationsTable);
		}
	}

} 