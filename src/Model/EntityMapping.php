<?php

namespace movi\Model;

use movi\InvalidArgumentException;
use Nette\Object;

class EntityMapping extends Object
{

	/**
	 * table => Entity class
	 *
	 * @var array
	 */
	private $entities = [];

	private $tables;


	public function __construct(array $entities)
	{
		foreach ($entities as $table => $class)
		{
			$this->registerEntity($table, $class);
		}
	}


	/**
	 * Returns table for entity
	 *
	 * @param $entityClass
	 * @return null
	 */
	public function getTable($entityClass)
	{
		$entityClass = Mapper::checkEntityClass($entityClass);

		return isset($this->tables[$entityClass]) ? $this->tables[$entityClass] : NULL;
	}


	/**
	 * Returns entity for table
	 *
	 * @param $table
	 * @return mixed
	 * @throws InvalidArgumentException
	 */
	public function getEntityClass($table)
	{
		if (!isset($this->entities[$table])) {
			throw new InvalidArgumentException("Unknown table: $table");
		}

		return $this->entities[$table];
	}


	/**
	 * @param $table
	 * @param $class
	 * @throws \movi\InvalidArgumentException
	 */
	public function registerEntity($table, $class)
	{
		if (isset($this->entities[$table])) {
			throw new InvalidArgumentException("Table $table is already registered");
		}

		$class = Mapper::checkEntityClass($class);

		$this->tables[$class] = $table;
		$this->entities[$table] = $class;
	}


	public function getEntities()
	{
		return $this->entities;
	}


	public function getTables()
	{
		return $this->tables;
	}

} 