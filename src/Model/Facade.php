<?php

namespace movi\Model;

use LeanMapper\IMapper;
use Nette\Object;

abstract class Facade extends Object
{

	/** @var Repository */
	protected $repository;

	public $onInsert;

	public $onUpdate;

	public $onPersist;

	public $onDelete;


	public function __construct(Repository $repository)
	{
		$this->repository = $repository;
	}


	/**
	 * @param Entity $entity
	 * @return mixed
	 */
	public function persist(Entity $entity)
	{
		$this->onPersist($entity);
		$isUpdating = !$entity->isDetached();

		$result = $this->repository->persist($entity);

		if ($isUpdating) {
			$this->onUpdate($entity);
		} else {
			$this->onInsert($entity);
		}

		return $result;
	}


	/**
	 * @param Entity $entity
	 * @throws \LeanMapper\Exception\InvalidStateException
	 */
	public function delete(Entity $entity)
	{
		$this->onDelete($entity);

		$this->repository->delete($entity);
	}


	/**
	 * @param Query $query
	 * @return array|bool|int|mixed
	 */
	public function findAll(Query $query = NULL)
	{
		if ($query === NULL) {
			$query = new Query();
		}

		$query->switchMode($query::COLLECTION_MODE);

		return $this->find($query);
	}


	/**
	 * @param $id
	 * @return array|bool|int|mixed
	 */
	public function findOne($id)
	{
		$query = new Query();
		$query->where('@id = %i', $id);

		return $this->find($query);
	}


	/**
	 * @param Query $query
	 * @return array|bool|int|mixed
	 */
	public function find(Query $query)
	{
		return $this->repository->find($query);
	}


	/**
	 * @return \LeanMapper\Connection
	 */
	protected function getConnection()
	{
		return $this->repository->getConnection();
	}


    /**
     * @param $key
     * @param $value
     * @return array
     */
    public function fetchPairs($key, $value)
    {
        $rows = $this->findAll();
        $pairs = [];

        foreach ($rows as $row) $pairs[$row->{$key}] = $row->{$value};

        return $pairs;
    }


	/**
	 * @param Query|NULL $query
	 * @return int
	 */
    public function count(Query $query = NULL)
    {
        if (!$query) $query = new Query();
        $query->switchMode($query::COUNTER_MODE);

        return $this->find($query);
    }

} 