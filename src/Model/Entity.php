<?php

namespace movi\Model;

abstract class Entity extends \LeanMapper\Entity
{

	public function refresh()
	{
		$this->row->cleanReferencingRowsCache();
		$this->row->cleanReferencedRowsCache();
	}

} 