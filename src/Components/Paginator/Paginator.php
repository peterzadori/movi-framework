<?php

namespace movi\Components\Paginator;

use movi\Application\UI\Control;
use Nette\Utils\Paginator as NettePaginator;

class Paginator extends Control
{

	/** @var NettePaginator */
	private $paginator;

	/** @persistent */
	public $page;

	private $ajax = true;


	public function __construct()
	{
		$this->paginator = new NettePaginator();
	}


	public function beforeRender()
	{
		$paginator = $this->paginator;
		$page = $paginator->page;

		if ($paginator->pageCount < 2) {
			$steps = [$page];
		} else {
			$arr = range(max($paginator->firstPage, $page - 3), min($paginator->lastPage, $page + 3));
			$count = 4;
			$quotient = ($paginator->pageCount - 1) / $count;

			for ($i = 0; $i <= $count; $i++) {
				$arr[] = round($quotient * $i) + $paginator->firstPage;
			}

			sort($arr);
			$steps = array_values(array_unique($arr));
		}

		$this->template->steps = $steps;
		$this->template->paginator = $paginator;
		$this->template->ajax = $this->ajax;
	}


	public function getPaginator()
	{
		return $this->paginator;
	}


	/**
	 * @param  array
	 * @return void
	 */
	public function loadState(array $params)
	{
		parent::loadState($params);

		$this->paginator->page = $this->page;
	}


	/**
	 * @param $value
	 * @return $this
	 */
	public function setAjax($value)
	{
		$this->ajax = $value;

		return $this;
	}

} 