<?php

namespace movi\Components\ImageUploader;

use movi\Application\UI\Control;

class ImageUploader extends Control
{

    public $onUpload;


    public $allowedExtensions = ['.jpg', '.png', '.gif'];


    public function attached($presenter)
    {
        parent::attached($presenter);

        $this->redrawControl(NULL, false);
    }


    public function handleUpload()
    {
        $file = $this->presenter->request->files['file'];

        $this->onUpload($file);
    }


}