<?php

namespace movi\Components\Grid\Components\Columns;

use Grido\Components\Columns\Column;

class Image extends Column
{

    private $width = 200;

    private $height = 80;


    public function render($row)
    {
        $value = parent::render($row);

        $templateFactory = $this->getPresenter()->getTemplateFactory();
        $template = $templateFactory->createTemplate($this->getPresenter());

        $template->setFile(__DIR__ . '/ImageColumn.latte');
        $template->value = $value;
        $template->width = $this->width;
        $template->height = $this->height;

        return $template->render();
    }


    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

}