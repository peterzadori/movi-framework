<?php

namespace movi\Components\Grid\Components\Columns;

use Grido\Components\Columns\Text;
use Nette\Utils\Html;

class Boolean extends Text
{

    public function getCellPrototype($row = NULL)
    {
        $cell = parent::getCellPrototype($row = NULL);
        $cell->class[] = 'text-center';
        return $cell;
    }


    public function getHeaderPrototype()
    {
        $head = parent::getHeaderPrototype();
        $head->class[] = 'text-center';
        return $head;
    }


    protected function formatValue($value)
    {
        $icon = $value ? 'check' : 'times';

        return Html::el('i')->class("fa fa-$icon");
    }

} 