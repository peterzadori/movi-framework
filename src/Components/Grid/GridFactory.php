<?php

namespace movi\Components\Grid;

use Grido\Components\Filters\Filter;
use Grido\DataSources\IDataSource;
use Grido\Translations\FileTranslator;
use Nette\Object;

abstract class GridFactory extends Object
{

	public function create()
	{
        $grid = new Grid();

        $grid->filterRenderType = Filter::RENDER_INNER;

		$this->configure($grid);

        $grid->onRender[] = function(Grid $grid) {
            //$grid->setTranslator(new FileTranslator('sk'));
            //$grid->setTemplateFile(__DIR__ . '/Grid.latte');
        };

		return $grid;
	}


	abstract protected function configure(Grid $grid);

} 