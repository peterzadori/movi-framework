<?php

namespace movi\Components\Grid;

use movi\Components\Grid\Components\Columns\Boolean;
use movi\Components\Grid\Components\Columns\Image;

class Grid extends \Grido\Grid
{

    public function addColumnBoolean($name, $label)
    {
        $column = new Boolean($this, $name, $label);

        return $column;
    }


    public function addImageColumn($name, $label, $width = NULL)
    {
        $column = new Image($this, $name, $label);

        if ($width) $column->setWidth($width);

        return $column;
    }

} 