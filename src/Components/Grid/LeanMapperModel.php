<?php

namespace movi\Components\Grid;

use Grido\Components\Filters\Condition;
use Grido\DataSources\IDataSource;
use movi\Model\Facade;
use movi\Model\Query;

class LeanMapperModel implements IDataSource
{

    /**
     * @var Facade
     */
    private $facade;

    /**
     * @var Query
     */
    private $query;


    public function __construct(Facade $facade, Query $query = NULL)
    {
        $this->facade = $facade;
        $this->query = $query ? $query : new Query();
    }


    /**
     * @return int
     */
    public function getCount()
    {
        $this->query->switchMode(Query::COUNTER_MODE);

        return $this->facade->find($this->query);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->facade->findAll($this->query);
    }

    /**
     * @param Condition[] $conditions
     * @return void
     */
    public function filter(array $conditions)
    {
        foreach ($conditions as $condition)
        {
            call_user_func_array(array($this->query, 'where'), $condition->__toArray('@'));
        }
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return void
     */
    public function limit($offset, $limit)
    {
        $this->query->offset($offset)->limit($limit);
    }

    /**
     * @param array $sorting
     * @return void
     */
    public function sort(array $sorting)
    {
        foreach ($sorting as $column => $sort)
        {
            $this->query->orderBy('@' . $column);

            $sort === 'ASC' ? $this->query->asc() : $this->query->desc();
        }
    }

    /**
     * @param mixed $column
     * @param array $conditions
     * @param int $limit
     * @return array
     */
    public function suggest($column, array $conditions, $limit)
    {

    }

}