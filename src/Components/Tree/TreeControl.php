<?php

namespace movi\Components\Tree;

use movi\Application\UI\Control;
use movi\Tree\Tree;

class TreeControl extends Control
{

    /**
     * @var Tree
     */
    private $tree;

    private $nodeTypes = [];

    public $buttonAdd = 'Pridať';

    public $noNodesNotice = 'Kliknutím na Pridať vytvoríte nový uzol.';

    public $dropdownHeader = '';


    public $onSelect;

    public $onAdd;

    public $onMove;


    public function __construct(Tree $tree)
    {
        $this->tree = $tree;
    }


    public function handleSelect($node)
    {
        $node = $this->tree->getNode($node);

        $this->onSelect($node);
    }


    public function handleAdd($type = NULL)
    {
        $this->onAdd($type);
    }


    public function handleMove()
    {
        $node = $this->presenter->request->getParameter('node');
        $parent = $this->presenter->request->getParameter('parent');
        $old_parent = $this->presenter->request->getParameter('old_parent');
        $position = $this->presenter->request->getParameter('position');

        $node = $this->tree->getNode($node);
        $parent = $this->tree->getNode($parent);
        $old_parent = $this->tree->getNode($old_parent);

        $this->tree->moveNode($node, $parent, $old_parent, $position);

        $this->onMove($node, $parent);
    }


    public function beforeRender()
    {
        $this->template->nodes = $this->tree->getRoot()->children;
        $this->template->nodeTypes = $this->nodeTypes;
    }


    public function addNodeType($key, $label)
    {
        $this->nodeTypes[$key] = $label;

        return $this;
    }

}