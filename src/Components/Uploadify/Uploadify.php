<?php

namespace movi\Components\Uploadify;

use movi\Application\UI\Control;

class Uploadify extends Control
{

	public $onRefresh;

	public $onUpload;

	/** @var array */
	private $mimeTypes = array();


	public function handleUpload()
	{
		$params = $this->presenter->request->post;
		$file = $this->presenter->request->files['file'];

		$this->onUpload($file, $params);

		$this->presenter->terminate();
	}


	public function handleRefresh()
	{
		$this->onRefresh();
	}


	public function beforeRender()
	{
		$this->template->rename = true;
		$this->template->mimeTypes = $this->mimeTypes;
	}


	/**
	 * @param $title
	 * @param array $extensions
	 * @return $this
	 */
	public function addMimeType($title, array $extensions)
	{
		$this->mimeTypes[] = array(
			'title' => $title,
			'extensions' => implode(',', $extensions)
		);

		return $this;
	}

} 