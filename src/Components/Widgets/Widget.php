<?php

namespace movi\Components\Widgets;

use movi\Application\UI\Control;

abstract class Widget extends Control
{

	/** @var array */
	protected $defaultSettings = array();

	/** @var array */
	protected $settings = array();


	/**
	 * @param array $settings
	 * @return $this
	 */
	public function setSettings(array $settings)
	{
		$this->settings = $settings + $this->defaultSettings;

		return $this;
	}


	public function createTemplate()
	{
		$theme = $this->presenter->context->getByType('Packages\AppPackage\Themes\Theme');
		$template = parent::createTemplate();
		$reflection = $this->getReflection();
		$file = $reflection->getShortName() . '.latte';

		$files = array();
		$files[] = $theme->widgetsDir . "/$file";
		$files[] = dirname($reflection->fileName) . '/' . $reflection->getShortName() . '.latte';

		foreach ($files as $file)
		{
			if (file_exists($file)) {
				$template->setFile($file);

				break;
			}
		}

		return $template;
	}


    /**
     * For back-end dashboard
     */
    public function renderDashboard()
    {

    }

} 