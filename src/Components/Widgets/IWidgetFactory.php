<?php

namespace movi\Components\Widgets;

interface IWidgetFactory
{

	public function create();

} 