<?php

namespace movi\Components\Widgets;

use movi\InvalidArgumentException;
use movi\ParamService;
use Nette\Object;

final class WidgetManager extends Object
{

	/** @var array */
	private $settings;

	/** @var IWidgetFactory[] */
	private $widgets;


	public function __construct(ParamService $paramService)
	{
		$this->settings = $paramService->getParam('widgets');
	}


	/**
	 * @param $name
	 * @param IWidgetFactory $factory
	 * @throws \movi\InvalidArgumentException
	 */
	public function addWidget($name, IWidgetFactory $factory)
	{
		if (isset($this->widgets[$name])) {
			throw new InvalidArgumentException("Widget $name is already added.");
		}

		$this->widgets[$name] = $factory;
	}


	/**
	 * @param $name
	 * @return IWidgetFactory
	 * @throws \movi\InvalidArgumentException
	 */
	public function getWidget($name)
	{
		if (!isset($this->widgets[$name])) {
			throw new InvalidArgumentException("Widget $name does not exist.");
		}

		return $this->widgets[$name];
	}


    /**
     * @return IWidgetFactory[]
     */
    public function getWidgets()
    {
        return $this->widgets;
    }


	/**
	 * @param null $widget
	 * @return array
	 */
	public function getSettings($widget = NULL)
	{
		if ($widget === NULL) {
			return $this->settings;
		}

		if (isset($this->settings[$widget])) {
			return $this->settings[$widget];
		}

		return array();
	}


} 