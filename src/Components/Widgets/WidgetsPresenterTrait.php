<?php

namespace movi\Components\Widgets;

use movi\InvalidArgumentException;
use Nette\Application\UI\Multiplier;

trait WidgetsPresenterTrait
{

    /**
     * @var WidgetManager
     */
    protected $widgetManager;

    public function injectWidgetManager(WidgetManager $widgetManager)
    {
        $this->widgetManager = $widgetManager;
    }


    /**
     * @return \movi\Components\Widgets\Widget
     */
    protected function createComponentWidgets()
    {
        return new Multiplier(function($name) {
            $factory = $this->widgetManager->getWidget($name);
            $widget = $factory->create();

            if (!($widget instanceof Widget)) {
                throw new InvalidArgumentException('Returned widget is not a widget.');
            }

            $widget->setSettings($this->widgetManager->getSettings($name));

            return $widget;
        });
    }

} 