<?php

namespace movi\Components\Head;

interface IHeadFactory
{

    /**
     * @return Head
     */
    public function create();

}