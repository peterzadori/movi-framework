<?php

namespace movi\Components\Head;

use movi\Application\UI\Control;
use Nette\Utils\Html;

class Head extends Control
{

    /**
     * @var string
     */
    private $pageName;

    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $metaTags = [];


    public function beforeRender()
    {
        $this->template->pageName = $this->pageName;
        $this->template->title = $this->title;
        $this->template->metaTags = $this->getMetaTags();
    }


    public function setPageName($name)
    {
        $this->pageName = $name;

        return $this;
    }


    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    public function addMetaTag($name, $content)
    {
        $this->metaTags[$name] = $content;

        return $this;
    }


    public function getMetaTags()
    {
        $metaTags = [];

        foreach ($this->metaTags as $name => $content)
        {
            $metaTags[] = Html::el('meta')->name($name)->content($content);
        }

        return $metaTags;
    }

}