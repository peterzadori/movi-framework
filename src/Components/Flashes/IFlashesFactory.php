<?php

namespace movi\Components\Flashes;

interface IFlashesFactory
{

	/** @return Flashes */
	public function create();

}