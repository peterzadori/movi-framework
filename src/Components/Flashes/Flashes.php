<?php

namespace movi\Components\Flashes;

use movi\Application\UI\Control;

final class Flashes extends Control
{

	public function attached($presenter)
	{
		parent::attached($presenter);

		if ($this->isAjax()) $this->redrawControl();
	}


	/**
	 * @param $message
	 * @param string $type
	 * @param null $title
	 * @return Flash
	 */
	public function flashMessage($message, $type = Flash::SUCCESS, $title = NULL, $close = true)
	{
		$id = $this->getParameterId('flash');
		$messages = $this->getPresenter()->getFlashSession()->$id;

		$flash = new Flash($message, $title, $type, $close);
		$messages[] = $flash;

		$this->getTemplate()->flashes = $messages;
		$this->getPresenter()->getFlashSession()->$id = $messages;

		return $flash;
	}

} 