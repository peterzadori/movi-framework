<?php

namespace movi\Components\Flashes;

use Nette\Object;

class Flash extends Object
{

	const SUCCESS = 'alert-success',
		ERROR = 'alert-danger',
		INFO = 'alert-info',
		WARNING = 'alert-warning';

	/** @var string */
	public $title;

	/** @var string */
	public $message;

	/** @var string */
	public $type;

	/** @var bool */
	public $close;


	public function __construct($message, $title = NULL, $type = self::SUCCESS, $close = true)
	{
		$this->message = $message;
		$this->title = $title;
		$this->type = $type;
		$this->close = $close;
	}

} 