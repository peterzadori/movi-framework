<?php

namespace movi\Components\Modal;

use movi\Application\UI\Control;
use Nette\Utils\Callback;

class Modal extends Control
{

    /**
     * Body of modal
     *
     * @var Control|callable
     */
    private $body;

    private $size;

    /**
     * @var string
     */
    private $title;


    public function __construct($body, $size = '')
    {
        $this->body = $body;
        $this->size = $size;
    }


    /**
     * Shows modal
     */
    public function showModal()
    {
        $this->presenter->payload->modal = 'modal-' . $this->getName();
        $this->template->showBody = true;

        $this->redrawControl('title');
        $this->redrawControl('body');
    }


    /**
     * Hides modal
     */
    public function hideModal()
    {
        $this->presenter->payload->modal = NULL;
        $this->template->showBody = false;

        $this->redrawControl('body');
    }


    public function attached($presenter)
    {
        parent::attached($presenter);

        if ($this->isAjax()) {
            $this->template->showBody = true;
            $this->redrawControl('body');
        }
    }


    public function beforeRender()
    {
        $this->template->title = $this->title;
        $this->template->size = $this->size;
    }


    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }


    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }


    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    /**
     * @return callable|mixed|Control
     */
    protected function createComponentBody()
    {
        if (is_callable($this->body)) {
            return Callback::invoke($this->body);
        } else {
            return $this->body;
        }
    }

}