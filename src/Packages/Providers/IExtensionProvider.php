<?php

namespace movi\Packages\Providers;

interface IExtensionProvider
{

    public function getExtensions();

} 