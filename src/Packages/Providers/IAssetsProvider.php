<?php

namespace movi\Packages\Providers;

interface IAssetsProvider
{

	public function getAssets();

} 