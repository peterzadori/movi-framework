<?php

namespace movi\Packages\Providers;

interface IMappingProvider
{

    public function getMappings();

} 