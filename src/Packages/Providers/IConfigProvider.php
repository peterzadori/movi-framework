<?php

namespace movi\Packages\Providers;

interface IConfigProvider
{

    public function getConfigFiles();

} 