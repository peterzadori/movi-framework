<?php

namespace movi\Files;

use Nette\Utils\FileSystem;

final class Helpers
{

	public static function ensureDir($dir)
	{
		FileSystem::createDir($dir);
	}


	public static function ensureFile($file)
	{
		if (!file_exists($file)) {
			self::ensureDir(dirname($file));

			file_put_contents($file, '');
		}
	}


	public static function getMetaFile($dir, $key)
	{
		return sprintf('%s/%s.meta', $dir, $key);
	}

} 