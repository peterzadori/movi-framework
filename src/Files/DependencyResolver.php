<?php

namespace movi\Files;

use movi\Files\Storages\Local;

class DependencyResolver
{

	/** @var \movi\Files\Storages\Local */
	private $storage;

	/** @var array */
	private $dependencies = array();


	public function __construct(Local $storage)
	{
		$this->storage = $storage;

		$this->prepareDependencies();
	}


	/**
	 * @param File $file
	 * @param array $dependencies
	 * @return File[]
	 */
	public function resolve(File $file, &$dependencies = array())
	{
		if (isset($this->dependencies[$file->getKey()])) {
			foreach ($this->dependencies[$file->getKey()] as $dependency)
			{
				$dependencies[] = $dependency;

				$this->resolve($dependency, $dependencies);
			}
		}

		return $dependencies;
	}


	private function prepareDependencies()
	{
		$files = $this->storage->getFiles(false);

		foreach ($files as $file)
		{
			$dependencies = $file->getMetadata()->getDependencies();

			foreach (array_keys($dependencies) as $dependency)
			{
				$this->dependencies[$dependency][] = $file;
			}
		}
	}

} 