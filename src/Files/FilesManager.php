<?php

namespace movi\Files;

class FilesManager
{

	/** @var \movi\Files\IStorage */
	private $storage;


	public function __construct(IStorage $storage)
	{
		$this->storage = $storage;
	}


	/**
	 * @param File $file
	 * @param null $key
	 */
	public function write(File $file, $key = NULL)
	{
		$this->storage->write($file, $key);
	}


	/**
	 * @param $key
	 * @param bool $readContent
	 * @return File
	 */
	public function read($key, $readContent = true)
	{
		return $this->storage->read($key, $readContent);
	}


	/**
	 * @param $key
	 */
	public function remove($key)
	{
		$this->storage->remove($key);
	}

} 