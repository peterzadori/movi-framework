<?php

namespace movi\Files;

class Metadata
{

	/** @var string */
	private $key;

	/** @var array */
	private $dependencies = array();

	public $data;


	public function getKey()
	{
		return $this->key;
	}


	public function setKey($key)
	{
		$this->key = $key;

		return $this;
	}


	/**
	 * @param File $file
	 * @return $this
	 */
	public function addDependency(File $file)
	{
		$this->dependencies[$file->getKey()] = true;

		return $this;
	}


	/**
	 * @param File $file
	 * @return $this
	 */
	public function removeDependency(File $file)
	{
		if (array_key_exists($file->getKey(), $this->dependencies)) {
			unset($this->dependencies[$file->getKey()]);
		}

		return $this;
	}


	public function getDependencies()
	{
		return $this->dependencies;
	}

} 