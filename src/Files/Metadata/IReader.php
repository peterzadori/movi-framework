<?php

namespace movi\Files\Metadata;

use movi\Files\Metadata;

interface IReader
{

	/**
	 * @param $key
	 * @return Metadata
	 */
	public function read($key);

} 