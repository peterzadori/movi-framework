<?php

namespace movi\Files\Metadata;

use movi\Files\Helpers;
use movi\Files\Metadata;
use movi\Utils\FileSystem;

class Writer implements IWriter
{

	/** @var string */
	private $dir;


	public function __construct($dir)
	{
		$this->dir = $dir;

		Helpers::ensureDir($dir);
	}


	public function write(Metadata $metadata)
	{
		$target = Helpers::getMetaFile($this->dir, $metadata->getKey());

		FileSystem::write($target, serialize($metadata));
	}


	public function remove(Metadata $metadata)
	{
		$target = Helpers::getMetaFile($this->dir, $metadata->getKey());

		FileSystem::delete($target);
	}

} 