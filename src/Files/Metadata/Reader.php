<?php

namespace movi\Files\Metadata;

use movi\Files\Helpers;
use movi\Files\Metadata;
use Nette\Utils\FileSystem;

class Reader implements IReader
{

	private $dir;

	private $cached = [];


	public function __construct($dir)
	{
		$this->dir = $dir;

		FileSystem::createDir($dir);
	}


	public function read($key)
	{
		if (!array_key_exists($key, $this->cached)) {
			$target = Helpers::getMetaFile($this->dir, $key);
			$metadata = new Metadata();

			if (file_exists($target)) {
				$metadata = unserialize(file_get_contents($target));
			}

			$this->cached[$key] = $metadata;
		}

		return $this->cached[$key];
	}

} 