<?php

namespace movi\Files\Metadata;

use movi\Files\Metadata;

interface IWriter
{

	public function write(Metadata $metadata);

	public function remove(Metadata $metadata);

} 