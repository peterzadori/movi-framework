<?php

namespace movi\Files\Utils;

use movi\Files\File;
use movi\Files\FilesManager;
use Nette\Utils\Image;
use Nette\Utils\ImageException;
use Nette\Utils\Strings;

class ThumbnailCreator
{

	/**
	 * @var FilesManager
	 */
	private $filesManager;


	public function __construct(FilesManager $filesManager)
	{
		$this->filesManager = $filesManager;
	}


	public function create($key, $width = NULL, $height = NULL, $flag = 'exact')
	{
		$file = $this->filesManager->read($key, false);

		if ($file !== NULL) {
			if ($height === NULL) $height = $width;

			$flag = Strings::upper($flag);
			$thumbKey = self::generateKey($key, $width, $height, $flag);
			$thumb = $this->filesManager->read($thumbKey);

			if ($thumb === NULL) {
				// Create a thumbnail
				try {
					$image = Image::fromFile($file->getPath());
					$image->resize($width, $height, constant("Nette\\Utils\\Image::$flag"));

					$thumb = new File($file->getName(), $image->toString($image::JPEG, 100));
					$thumb->setNamespace('thumbs');
					$thumb->addDependency($file);

					$this->filesManager->write($thumb, $thumbKey);
				} catch (ImageException $e) {

				}
			}

			return $thumb;
		}
	}


	private static function generateKey($key, $width, $height, $flag)
	{
		return sprintf('%dx%d-%s-%s', $width, $height, $key, $flag);
	}

} 