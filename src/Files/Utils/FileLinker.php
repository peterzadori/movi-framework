<?php

namespace movi\Files\Utils;

use movi\Files\FilesManager;
use movi\ParamService;
use Nette\Http\Request;

class FileLinker
{

	/** @var \movi\Files\FilesManager */
	private $filesManager;

	/** @var \movi\ParamService */
	private $paramService;

	/** @var \Nette\Http\Request */
	private $request;


	public function __construct(FilesManager $filesManager, ParamService $paramService, Request $request)
	{
		$this->filesManager = $filesManager;
		$this->paramService = $paramService;
		$this->request = $request;
	}


	public function getRelativePath($key)
	{
		$path = $this->getAbsolutePath($key);

		if ($path !== NULL) {
			$wwwDir = $this->paramService->getParam('wwwDir');
			$relativePath = substr($path, strlen($wwwDir) + 1);

			return $this->request->url->baseUrl . $relativePath;
		}
	}


	public function getAbsolutePath($key)
	{
		$file = $this->filesManager->read($key, false);

		if ($file !== NULL) {
			return $file->getPath();
		}
	}

} 