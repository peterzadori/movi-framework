<?php

namespace movi\Files\Storages;

use movi\Files\File;
use movi\Model\Facades\FilesFacade;
use movi\Utils\FileSystem;
use Nette\Utils\Json;
use Nette\Utils\Random;

final class LocalFiles
{

	private $filesFacade;

	/** @var array */
	private $files;


	public function __construct(FilesFacade $filesFacade)
	{
		$this->filesFacade = $filesFacade;
	}


	/**
	 * @param File $file
	 */
	public function write(File $file)
	{
		$this->getFiles();

		$this->files[$file->getKey()] = $file->getFile();
		$this->filesFacade->create($file);
	}


	/**
	 * @param $key
	 * @return string
	 */
	public function get($key)
	{
		$this->getFiles();

		if (array_key_exists($key, $this->files)) {
			return $this->files[$key];
		}

		return false;
	}


	/**
	 * @param File $file
	 * @return bool
	 */
	public function has(File $file)
	{
		$this->getFiles();

		return array_search($file->getFile(), $this->files) !== false;
	}


	/**
	 * @param File $file
	 */
	public function remove(File $file)
	{
		$this->getFiles();

		unset($this->files[$file->getKey()]);
		$this->filesFacade->remove($file);
	}


	public function getFiles()
	{
		if ($this->files === NULL) {
			$this->files = $this->filesFacade->getFiles();
		}

		return $this->files;
	}


	/**
	 * @param File $file
	 */
	public function checkFile(File $file)
	{
		$this->getFiles();

		do {
			$file->setName(Random::generate(5) . '-' . $file->getName());
		} while (array_search($file->getFile(), $this->files));
	}


	/**
	 * @param File $file
	 * @return string
	 */
	public function generateKey(File $file)
	{
		$this->getFiles();

		do {
			$key = sha1(Random::generate(5) . $file->getName());
		} while (array_key_exists($key, $this->files));

		return $key;
	}

} 