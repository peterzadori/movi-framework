<?php

namespace movi\Files\Storages;

use movi\Files\DependencyResolver;
use movi\Files\File;
use movi\Files\IStorage;
use movi\Files\Metadata\IReader;
use movi\Files\Metadata\IWriter;
use movi\Utils\FileSystem;
use Nette\Utils\Strings;

class Local implements IStorage
{

	/** @var string */
	private $dir;

	/** @var \movi\Files\Storages\LocalFiles */
	private $localFiles;

	/** @var \movi\Files\Metadata\IReader */
	private $reader;

	/** @var \movi\Files\Metadata\IWriter */
	private $writer;

	private $cached = [];


	public function __construct($dir, IReader $reader, IWriter $writer, LocalFiles $localFiles)
	{
		$this->dir = $dir;

		FileSystem::createDir($dir);

		$this->localFiles = $localFiles;
		$this->reader = $reader;
		$this->writer = $writer;
	}


	/**
	 * @param File $file
	 * @param null $key
	 */
	public function write(File $file, $key = NULL)
	{
		if (!$this->localFiles->has($file)) {
			$this->localFiles->checkFile($file);
		}

		if ($key === NULL && $file->getKey() === NULL) {
			$file->setKey($this->localFiles->generateKey($file));
		} elseif ($key !== NULL) {
			$file->setKey($key);
		}

		$file->setStorage($this);

		$target = sprintf('%s/%s', $this->dir, $file->getFile());

		FileSystem::createDir(dirname($target));
		FileSystem::write($target, $file->getContent());

		$this->localFiles->write($file);
		$this->writer->write($file->getMetadata());
	}


	/**
	 * @param $key
	 * @param bool $readContent
	 * @return File|NULL
	 */
	public function read($key, $readContent = true)
	{
		if ($source = $this->localFiles->get($key)) {
			$metadata = $this->reader->read($key);
			$namespace = NULL;
			$name = $source;

			if (Strings::contains($source, '/')) {
				$namespaces = explode('/', $source);
				$name = array_pop($namespaces);

				$namespace = implode('/', $namespaces);
			}

			$file = new File($name, NULL, $metadata);
			$file->setStorage($this);

			if ($namespace !== NULL) $file->setNamespace($namespace);

			return $this->cached[$key] = $file;
		}
	}


	/**
	 * @param $key
	 * @return bool
	 */
	public function remove($key)
	{
		$file = $this->read($key, false);

		if (!$file) {
			return false;
		}

		$dependencyResolver = new DependencyResolver($this);
		$dependencies = $dependencyResolver->resolve($file);

		$dependencies[] = $file;

		foreach ($dependencies as $dependency)
		{
			$this->removeFile($dependency);
			$this->writer->remove($dependency->getMetadata());
			$this->localFiles->remove($dependency);
		}

		return true;
	}


	/**
	 * @param File $file
	 * @return string
	 */
	public function getPath(File $file)
	{
		return sprintf('%s/%s', $this->dir, $file->getFile());
	}


	public function removeFile(File $file)
	{
		$target = sprintf('%s/%s', $this->dir, $file->getFile());

		FileSystem::delete($target);
	}


	/**
	 * @param bool $readContent
	 * @return File[]
	 */
	public function getFiles($readContent = true)
	{
		$keys = $this->localFiles->getFiles();
		$files = [];

		foreach (array_keys($keys) as $key)
		{
			$files[] = $this->read($key, $readContent);
		}

		return $files;
	}

} 