<?php

namespace movi\Files;

interface IStorage
{

	public function write(File $file, $key = NULL);

	/**
	 * @param $key
	 * @param bool $readContent
	 * @return mixed
	 */
	public function read($key, $readContent = true);

	public function remove($key);

	public function getPath(File $file);

} 