<?php

namespace movi\Files;

class File
{

	/** @var string */
	private $key;

	/** @var string */
	private $name;

	/** @var string */
	private $content;

	/** @var string */
	private $namespace;

	/** @var \movi\Files\Metadata */
	private $metadata;

	private $storage;


	public function __construct($name, $content = NULL, Metadata $metadata = NULL)
	{
		$this->name = $name;
		$this->content = $content;
		$this->metadata = ($metadata === NULL) ? new Metadata() : $metadata;
		$this->key = $this->metadata->getKey();
	}


	public function getKey()
	{
		return $this->key;
	}


	public function setKey($key)
	{
		$this->key = $key;
		$this->metadata->setKey($key);

		return $this;
	}


	public function getName()
	{
		return $this->name;
	}


	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}


	public function getContent()
	{
		if ($this->content === NULL && file_exists($this->getPath())) {
			$this->content = file_get_contents($this->getPath());
		}

		return $this->content;
	}


	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}


	public function getMetadata()
	{
		return $this->metadata;
	}


	public function setNamespace($ns)
	{
		$this->namespace = $ns;

		return $this;
	}


	public function getNamespace()
	{
		return $this->namespace;
	}


	public function setStorage(IStorage $storage)
	{
		$this->storage = $storage;

		return $this;
	}


	public function getFile()
	{
		return ($this->namespace !== NULL) ? sprintf('%s/%s', $this->namespace, $this->name) : $this->name;
	}


	public function getPath()
	{
		return $this->storage->getPath($this);
	}


	public function getSize()
	{
		return filesize($this->getPath());
	}


	/**
	 * @param File $file
	 * @return $this
	 */
	public function addDependency(self $file)
	{
		$this->metadata->addDependency($file);

		return $this;
	}


	/**
	 * @param File $file
	 * @return $this
	 */
	public function removeDependency(self $file)
	{
		$this->metadata->removeDependency($file);

		return $this;
	}

} 