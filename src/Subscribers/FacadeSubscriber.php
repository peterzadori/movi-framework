<?php

namespace movi\Subscribers;

use Kdyby\Events\Subscriber;
use LeanMapper\IMapper;
use movi\Files\FilesManager;
use movi\Model\Entities\FileEntity;
use movi\Model\Entity;

class FacadeSubscriber implements Subscriber
{

	/**
	 * @var FilesManager
	 */
	private $filesManager;

	/**
	 * @var IMapper
	 */
	private $mapper;


	public function __construct(FilesManager $filesManager, IMapper $mapper)
	{
		$this->filesManager = $filesManager;
		$this->mapper = $mapper;
	}


	public function getSubscribedEvents()
	{
		return array('movi\Model\Facade::onDelete');
	}


	public function onDelete(Entity $entity)
	{
		if ($entity instanceof FileEntity) {
			/** @var Entity $entity */
			$properties = $entity->getReflection($this->mapper)->getEntityProperties();

			foreach ($properties as $property)
			{
				if ($property->hasCustomFlag('file')) {
					$file = $entity->{$property->getName()};

					if ($file !== NULL) {
						$this->filesManager->remove($file);
					}
				}
			}
		}
	}

} 