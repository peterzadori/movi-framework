<?php

namespace movi\Subscribers;

use Kdyby\Events\Subscriber;
use LeanMapper\Connection;
use LeanMapper\Result;

class EntitySerializationSubscriber implements Subscriber
{

    /**
     * @var Connection
     */
    private $connection;


    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }


    public function getSubscribedEvents()
    {
        return ['Nette\\DI\\Container::onInitialize'];
    }


    public function onInitialize()
    {
        Result::enableSerialization($this->connection);
    }

}