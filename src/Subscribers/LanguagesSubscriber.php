<?php

namespace movi\Subscribers;

use Kdyby\Events\Subscriber;
use movi\Localization\Languages;
use movi\Localization\Translator;

class LanguagesSubscriber implements Subscriber
{

    /**
     * @var Languages
     */
    private $languages;

    /**
     * @var Translator
     */
    private $translator;


    public function __construct(Languages $languages, Translator $translator)
    {
        $this->languages = $languages;
        $this->translator = $translator;
    }


    public function getSubscribedEvents()
    {
        return ['Nette\Application\Application::onStartup'];
    }


    public function onStartup()
    {
        $default = $this->languages->getDefaultLanguage();

        if ($default) {
            $this->translator->setLanguage($default);
        }

        $this->translator->setLanguages($this->languages->findAll());
    }

}